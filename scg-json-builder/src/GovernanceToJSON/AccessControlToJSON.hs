{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE UndecidableInstances  #-}

module  GovernanceToJSON.AccessControlToJSON
                (   main
                ,   accessControlRefUTxODatum
                ,   accessControlGovernorDatum
                ,   arena1'AccessControlRefUTxOManagementRedeemer
                ,   arena2'AccessControlRefUTxOManagementRedeemer
                ,   arena3'AccessControlRefUTxOManagementRedeemer
                ,   colosseumRefUTxOManagementRedeemer
                ,   designateGateKeeperRedeemer
                ,   revokeGateKeeperRedeemer
                ,   revokeGovernorRedeemer
                ,   revokeAccessControlRefUTxORedeemer
                ,   accessControlDebuggerRedeemer
                )
                    where

import           Cardano.Api                        (ScriptDataJsonSchema (ScriptDataJsonDetailedSchema),
                                                     scriptDataToJson)
import           Cardano.Api.Shelley                (fromPlutusData)
import           Data.Aeson                         (encode)
import           Data.ByteString.Lazy               (writeFile)
import           Plutus.V1.Ledger.Address           (Address (..))
import           Plutus.V1.Ledger.Credential        (Credential (PubKeyCredential),
                                                     StakingCredential (StakingHash))
import           Plutus.V1.Ledger.Crypto            (PubKeyHash)
import           Plutus.V1.Ledger.Scripts           (ValidatorHash)
import           Plutus.V1.Ledger.Time              (POSIXTime)
import           Plutus.V1.Ledger.Value             (CurrencySymbol)
import           PlutusTx                           (ToData, toData)
import           PlutusTx.Prelude                   (Bool (..), Maybe (..), ($),
                                                     (.), (<>))
import qualified Prelude                            as Haskell
import           System.Directory                   (createDirectoryIfMissing)
import           System.FilePath.Posix              ((<.>), (</>))

import           GovernanceTypes.AccessControlTypes (AccessControlAction (..),
                                                     AccessControlDatum (..),
                                                     Arena1'DomainInfo (..),
                                                     Arena2'DomainInfo (..),
                                                     Arena3'DomainInfo (..),
                                                     ColosseumDomainInfo (..),
                                                     TreasuryInfo (..))
import           Validators                         (a1'DATCurrencySymbol,
                                                     a1'GATCurrencySymbol,
                                                     arena1'GateKeeperValidatorHash,
                                                     arena1'ProxyValidatorHash,
                                                     arena1'RefUTxOIssuerValidatorHash)

{-------------------GOVERNOR INFORMATION----------------------}

governorPKH' :: PubKeyHash
governorPKH' = "318642c25f4d2dc3c59e60e3867991c8b9bed2cced13b5d2a127523e"

governorSession' :: POSIXTime
governorSession' = 1675376832000

{-------------------------------------------------------------------}

{-------------------EMPEROR TREASURY INFORMATION---------------------}

emperorTreasuryPaymentPKH :: Credential
emperorTreasuryPaymentPKH = PubKeyCredential "fcdf1eccbb986b30d96bea91cfe4a7e5316abd0dc00f2d063ed2b607"

emperorTreasuryStakePKH :: Maybe StakingCredential
emperorTreasuryStakePKH = Just $ StakingHash $ PubKeyCredential "d337eb6b5d8d728d5be35b7da0c3b5a6b896836bbf465050ad0b857f"

emperorTreasuryAddress' :: Address
emperorTreasuryAddress' = Address emperorTreasuryPaymentPKH emperorTreasuryStakePKH

{-------------------------------------------------------------------}

{-------------------REPUBLIC TREASURY INFORMATION---------------------}

republicTreasuryPaymentPKH :: Credential
republicTreasuryPaymentPKH = PubKeyCredential "a512a21ba616d7b8897445ad269d78f27185a3e8aa553774d36fa967"

republicTreasuryStakePKH :: Maybe StakingCredential
republicTreasuryStakePKH = Just $ StakingHash $ PubKeyCredential "1f7455d9dd124a036d4cfa09b8d7c5c231927f6d52a4c59d3239c450"

republicTreasuryAddress' :: Address
republicTreasuryAddress' = Address republicTreasuryPaymentPKH republicTreasuryStakePKH

{-------------------------------------------------------------------}

{--------------------ARENA 1 DOMAIN INFORMATION---------------------}

arena1'RefUTxOIssuerVH' :: ValidatorHash
arena1'RefUTxOIssuerVH' = arena1'RefUTxOIssuerValidatorHash

arena1'GateKeeperVH' :: ValidatorHash
arena1'GateKeeperVH' = arena1'GateKeeperValidatorHash

arena1'ProxyVH' :: ValidatorHash
arena1'ProxyVH' = arena1'ProxyValidatorHash

currencySymbolOfA1'DAT' :: CurrencySymbol
currencySymbolOfA1'DAT' = a1'DATCurrencySymbol

currencySymbolOfA1'GAT' :: CurrencySymbol
currencySymbolOfA1'GAT' = a1'GATCurrencySymbol

{-------------------------------------------------------------------}

{------------------------------DATUMS-------------------------------}

accessControlRefUTxODatum :: AccessControlDatum
accessControlRefUTxODatum =
    AccessControlRefUTxODatum
        (    TreasuryInfo
                emperorTreasuryAddress'
                republicTreasuryAddress'
        )
        (   Arena1'DomainInfo
                arena1'RefUTxOIssuerVH'
                arena1'GateKeeperVH'
                arena1'ProxyVH'
                currencySymbolOfA1'DAT'
                currencySymbolOfA1'GAT'
        )
        (   Arena2'DomainInfo
                arena1'RefUTxOIssuerVH'
                arena1'GateKeeperVH'
                arena1'ProxyVH'
                currencySymbolOfA1'DAT'
                currencySymbolOfA1'GAT'
        )
        (   Arena3'DomainInfo
                arena1'RefUTxOIssuerVH'
                arena1'GateKeeperVH'
                arena1'ProxyVH'
                currencySymbolOfA1'DAT'
                currencySymbolOfA1'GAT'
        )
        (   ColosseumDomainInfo
                arena1'RefUTxOIssuerVH'
                arena1'GateKeeperVH'
                arena1'ProxyVH'
                currencySymbolOfA1'DAT'
                currencySymbolOfA1'GAT'
        )

accessControlGovernorDatum :: AccessControlDatum
accessControlGovernorDatum =
    GovernorDatum
        governorPKH'
        governorSession'

{-------------------------------------------------------------------}

{-----------------------------REDEEMERS-----------------------------}

arena1'AccessControlRefUTxOManagementRedeemer :: AccessControlAction
arena1'AccessControlRefUTxOManagementRedeemer = Arena1'AccessControlRefUTxOManagement

arena2'AccessControlRefUTxOManagementRedeemer :: AccessControlAction
arena2'AccessControlRefUTxOManagementRedeemer = Arena2'AccessControlRefUTxOManagement

arena3'AccessControlRefUTxOManagementRedeemer :: AccessControlAction
arena3'AccessControlRefUTxOManagementRedeemer = Arena3'AccessControlRefUTxOManagement

colosseumRefUTxOManagementRedeemer :: AccessControlAction
colosseumRefUTxOManagementRedeemer = ColosseumRefUTxOManagement

designateGateKeeperRedeemer :: AccessControlAction
designateGateKeeperRedeemer = DesignateGateKeeper

revokeGateKeeperRedeemer :: AccessControlAction
revokeGateKeeperRedeemer = RevokeGateKeeper

revokeGovernorRedeemer :: AccessControlAction
revokeGovernorRedeemer = RevokeGovernor

revokeAccessControlRefUTxORedeemer :: AccessControlAction
revokeAccessControlRefUTxORedeemer = RevokeAccessControlRefUTxO

accessControlDebuggerRedeemer :: AccessControlAction
accessControlDebuggerRedeemer = AccessControlDebugger

{-------------------------------------------------------------------}


writeJSON :: ToData a => Haskell.FilePath -> a -> Haskell.IO ()
writeJSON file = writeFile file . encode . scriptDataToJson ScriptDataJsonDetailedSchema . fromPlutusData . toData

main :: Haskell.IO ()
main = do
        let datDir          = "../dist/data/datums"
            redDir          = "../dist/data/redeemers"
            contractName    = "AccessControl"
        createDirectoryIfMissing True $ datDir </> contractName
        createDirectoryIfMissing True $ redDir </> contractName

        writeJSON (datDir </> contractName </> "access_control_ref_utxo_datum"  <.> "json") accessControlRefUTxODatum
        writeJSON (datDir </> contractName </> "access_control_governor_datum"  <.> "json") accessControlGovernorDatum

        writeJSON (redDir </> contractName </> "arena_1_ref_utxo_management_redeemer"       <.> "json") arena1'AccessControlRefUTxOManagementRedeemer
        writeJSON (redDir </> contractName </> "arena_2_ref_utxo_management_redeemer"       <.> "json") arena2'AccessControlRefUTxOManagementRedeemer
        writeJSON (redDir </> contractName </> "arena_3_ref_utxo_management_redeemer"       <.> "json") arena3'AccessControlRefUTxOManagementRedeemer
        writeJSON (redDir </> contractName </> "colosseum_ref_utxo_management_redeemer"     <.> "json") colosseumRefUTxOManagementRedeemer
        writeJSON (redDir </> contractName </> "designate_gate_keeper_redeemer"             <.> "json") designateGateKeeperRedeemer
        writeJSON (redDir </> contractName </> "revoke_gate_keeper_redeemer"                <.> "json") revokeGateKeeperRedeemer
        writeJSON (redDir </> contractName </> "revoke_governor_redeemer"                   <.> "json") revokeGovernorRedeemer
        writeJSON (redDir </> contractName </> "revoke_access_control_ref_utxo_redeemer"    <.> "json") revokeAccessControlRefUTxORedeemer
        writeJSON (redDir </> contractName </> "access_control_debugger_redeemer"           <.> "json") accessControlDebuggerRedeemer

        Haskell.putStrLn        "\n<--------------------------------------DONE------------------------------------------>"
        Haskell.putStrLn        "   ATTENTION:"
        Haskell.putStrLn    $   "       Datums location:    " <> Haskell.show ( datDir </> contractName)
        Haskell.putStrLn    $   "       Redeemers location: " <> Haskell.show (redDir </> contractName)
        Haskell.putStrLn        "<------------------------------------------------------------------------------------>\n"
