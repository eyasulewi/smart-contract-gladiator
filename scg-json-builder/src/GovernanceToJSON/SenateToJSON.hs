{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE UndecidableInstances  #-}

module GovernanceToJSON.SenateToJSON
        (   main
        ,   designationRedeemer
        ,   revocationRedeemer
        ,   senateDebuggerRedeemer
        )
            where

import           Cardano.Api                 (ScriptDataJsonSchema (ScriptDataJsonDetailedSchema),
                                              scriptDataToJson)
import           Cardano.Api.Shelley         (fromPlutusData)
import           Data.Aeson                  (encode)
import           Data.ByteString.Lazy        (writeFile)
import           PlutusTx                    (ToData, toData)
import           PlutusTx.Prelude            (Bool (..), ($), (.), (<>))
import qualified Prelude                     as Haskell
import           System.Directory            (createDirectoryIfMissing)
import           System.FilePath.Posix       ((<.>), (</>))

import           GovernanceTypes.SenateTypes (SenateAction (..))

{-----------------------------REDEEMERS-----------------------------}

designationRedeemer :: SenateAction
designationRedeemer = Designation

revocationRedeemer :: SenateAction
revocationRedeemer = Revocation

vetoRedeemer :: SenateAction
vetoRedeemer = Veto

senateDebuggerRedeemer :: SenateAction
senateDebuggerRedeemer = SenateDebugger

{--------------------------------------------------------------------}


writeJSON :: ToData a => Haskell.FilePath -> a -> Haskell.IO ()
writeJSON file = writeFile file . encode . scriptDataToJson ScriptDataJsonDetailedSchema . fromPlutusData . toData

main :: Haskell.IO ()
main = do
        let redDir          = "../dist/data/redeemers"
            contractName    = "Senate"
        createDirectoryIfMissing True $ redDir </> contractName

        writeJSON (redDir </> contractName </> "designation_redeemer"       <.> "json") designationRedeemer
        writeJSON (redDir </> contractName </> "revocation_redeemer"        <.> "json") revocationRedeemer
        writeJSON (redDir </> contractName </> "veto_redeemer"              <.> "json") vetoRedeemer
        writeJSON (redDir </> contractName </> "senate_debugger_redeemer"   <.> "json") senateDebuggerRedeemer

        Haskell.putStrLn        "\n<--------------------------------------DONE------------------------------------------>"
        Haskell.putStrLn        "   ATTENTION:"
        Haskell.putStrLn    $   "       Redeemers location: " <> Haskell.show (redDir </> contractName)
        Haskell.putStrLn        "<------------------------------------------------------------------------------------>\n"
