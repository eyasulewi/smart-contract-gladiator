{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE UndecidableInstances  #-}

module  ArenaOneToJSON.A1GateKeeperToJSON
                (   main
                ,   arena1'GateKeeperUndertakerAdminDatum
                ,   arena1'MasterDatum
                ,   validateArena1'ContractsAtMintingA1'GATRedeemer
                ,   validateArena1'ContractsAtVotingRedeemer
                ,   validateArena1'ContractsAtBattlefieldRedeemer
                ,   arena1'ContractsRevocationRedeemer
                ,   arena1'GateKeeperDebuggerRedeemer
                )
                    where

import           Cardano.Api                     (ScriptDataJsonSchema (ScriptDataJsonDetailedSchema),
                                                  scriptDataToJson)
import           Cardano.Api.Shelley             (fromPlutusData)
import           Data.Aeson                      (encode)
import           Data.ByteString.Lazy            (writeFile)
import           Plutus.V1.Ledger.Crypto         (PubKeyHash)
import           Plutus.V1.Ledger.Time           (POSIXTime)
import           PlutusTx                        (ToData, toData)
import           PlutusTx.Prelude                (Bool (..), ($), (.), (<>))
import qualified Prelude                         as Haskell
import           System.Directory                (createDirectoryIfMissing)
import           System.FilePath.Posix           ((<.>), (</>))

import           ArenaOneTypes.A1GateKeeperTypes (Arena1'GateKeeperAction (..),
                                                  Arena1'GateKeeperDatum (..))
import           GovernanceTypes.RBACTypes       (Assignment (..), Domain (..),
                                                  Role (..))


{--------------------MINTER ADMIN INFORMATION----------------------}

arena1'GateKeeperUndertakerPKH' :: PubKeyHash
arena1'GateKeeperUndertakerPKH' = "cdcc6941d223c4b6313e589d041d7ae658a8400271dc36a80f4c1d36"

arena1'GateKeeperUndertakerAdminAssignment :: Assignment
arena1'GateKeeperUndertakerAdminAssignment = Assignment Arena1 Undertaker

arena1'GateKeeperUndertakerAdminActivitySession :: POSIXTime
arena1'GateKeeperUndertakerAdminActivitySession = 1675376832000

{-------------------------------------------------------------------}

{----------------------ARENA MASTER INFORMATION---------------------}

arena1'MasterPKH' :: PubKeyHash
arena1'MasterPKH' = "31ee873ae53622e1d4fa6d225475c923472487ca6b85823265c29b88"

arena1'MasterAssignment' :: Assignment
arena1'MasterAssignment' = Assignment Arena1 ArenaMaster

arena1'MasterSession' :: POSIXTime
arena1'MasterSession' = 1675376832000

{-------------------------------------------------------------------}

{------------------------------DATUMS-------------------------------}

arena1'GateKeeperUndertakerAdminDatum :: Arena1'GateKeeperDatum
arena1'GateKeeperUndertakerAdminDatum =
    Arena1'GateKeeperAdminDatum
            arena1'GateKeeperUndertakerPKH'
            arena1'GateKeeperUndertakerAdminAssignment
            arena1'GateKeeperUndertakerAdminActivitySession

arena1'MasterDatum :: Arena1'GateKeeperDatum
arena1'MasterDatum =
    Arena1'GateKeeperAdminDatum
            arena1'MasterPKH'
            arena1'MasterAssignment'
            arena1'MasterSession'

{-------------------------------------------------------------------}

{-----------------------------REDEEMERS-----------------------------}

validateArena1'ContractsAtMintingA1'GATRedeemer :: Arena1'GateKeeperAction
validateArena1'ContractsAtMintingA1'GATRedeemer = ValidateArena1'ContractsAtMintingA1'GAT

validateArena1'ContractsAtVotingRedeemer :: Arena1'GateKeeperAction
validateArena1'ContractsAtVotingRedeemer = ValidateArena1'ContractsAtVoting

validateArena1'ContractsAtBattlefieldRedeemer :: Arena1'GateKeeperAction
validateArena1'ContractsAtBattlefieldRedeemer = ValidateArena1'ContractsAtBattlefield

arena1'ContractsRevocationRedeemer :: Arena1'GateKeeperAction
arena1'ContractsRevocationRedeemer = Arena1'GladiatorsRevocation

arena1'GateKeeperDebuggerRedeemer :: Arena1'GateKeeperAction
arena1'GateKeeperDebuggerRedeemer = Arena1'GateKeeperDebugger

{-------------------------------------------------------------------}


writeJSON :: ToData a => Haskell.FilePath -> a -> Haskell.IO ()
writeJSON file = writeFile file . encode . scriptDataToJson ScriptDataJsonDetailedSchema . fromPlutusData . toData

main :: Haskell.IO ()
main = do
        let datDir          = "../dist/data/datums"
            redDir          = "../dist/data/redeemers"
            contractName    = "A1GateKeeper"
        createDirectoryIfMissing True $ datDir </> contractName
        createDirectoryIfMissing True $ redDir </> contractName

        writeJSON (datDir </> contractName </> "arena_1_gate_keeper_undertaker_admin_datum" <.> "json") arena1'GateKeeperUndertakerAdminDatum
        writeJSON (datDir </> contractName </> "arena_1_master_datum"                       <.> "json") arena1'MasterDatum

        writeJSON (redDir </> contractName </> "validate_arena_1_contracts_at_minting_A1GAT_redeemer"   <.> "json") validateArena1'ContractsAtMintingA1'GATRedeemer
        writeJSON (redDir </> contractName </> "validate_arena_1_contracts_at_voting_redeemer"          <.> "json") validateArena1'ContractsAtVotingRedeemer
        writeJSON (redDir </> contractName </> "validate_arena_1_contracts_at_battlefield_redeemer"     <.> "json") validateArena1'ContractsAtBattlefieldRedeemer
        writeJSON (redDir </> contractName </> "arena_1_contracts_revocation_redeemer"                  <.> "json") arena1'ContractsRevocationRedeemer
        writeJSON (redDir </> contractName </> "arena_1_gate_keeper_redeemer"                           <.> "json") arena1'GateKeeperDebuggerRedeemer

        Haskell.putStrLn        "\n<--------------------------------------DONE------------------------------------------>"
        Haskell.putStrLn        "   ATTENTION:"
        Haskell.putStrLn    $   "       Datums location:    " <> Haskell.show ( datDir </> contractName)
        Haskell.putStrLn    $   "       Redeemers location: " <> Haskell.show (redDir </> contractName)
        Haskell.putStrLn        "<------------------------------------------------------------------------------------>\n"
