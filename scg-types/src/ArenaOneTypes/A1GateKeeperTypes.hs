
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE ImportQualifiedPost   #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}

module  ArenaOneTypes.A1GateKeeperTypes
            (   Arena1'GateKeeperParams (..)
            ,   Arena1'GateKeeperDatum  (..)
            ,   Arena1'GateKeeperAction (..)
            )
                where

import           GHC.Generics              (Generic)
import           Plutus.V1.Ledger.Crypto   (PubKeyHash)
import           Plutus.V1.Ledger.Scripts  (ValidatorHash)
import           Plutus.V1.Ledger.Time     (POSIXTime)
import           Plutus.V1.Ledger.Value    (CurrencySymbol)
import qualified PlutusTx
import           PlutusTx.Prelude          (Bool (..), Eq, (&&), (==))
import qualified Prelude                   as Haskell

import           GovernanceTypes.RBACTypes (Assignment)

data Arena1'GateKeeperParams = Arena1'GateKeeperParams
    {   accessControlVH              ::  ValidatorHash
    ,   currencySymbolOfA1'DAT       ::  CurrencySymbol
    ,   arena1'GateKeeperDebuggerPKH ::  PubKeyHash
    }
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

PlutusTx.makeLift ''Arena1'GateKeeperParams

data Arena1'GateKeeperDatum
    =   Arena1'GateKeeperAdminDatum
            {   arena1'GateKeeperAdminPKH        ::  PubKeyHash
            ,   arena1'GateKeeperAdminAssignment ::  Assignment
            ,   arena1'GateKeeperAdminSession    ::  POSIXTime
            }
        deriving
            (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

instance Eq Arena1'GateKeeperDatum where
    {-# INLINABLE (==) #-}
    Arena1'GateKeeperAdminDatum a b c == Arena1'GateKeeperAdminDatum a' b' c'
        |   a == a' && b == b' && c == c' = True
    _   ==  _   = False

PlutusTx.makeIsDataIndexed ''Arena1'GateKeeperDatum  [ ( 'Arena1'GateKeeperAdminDatum,    1 )]

data Arena1'GateKeeperAction
    =   ValidateArena1'ContractsAtMintingA1'GAT
    |   ValidateArena1'ContractsAtVoting
    |   ValidateArena1'ContractsAtBattlefield
    |   Arena1'GladiatorsRevocation
    |   Arena1'GateKeeperRevocation
    |   Arena1'GateKeeperDebugger
    deriving (Haskell.Show)

PlutusTx.makeIsDataIndexed ''Arena1'GateKeeperAction    [ ( 'ValidateArena1'ContractsAtMintingA1'GAT,   1 )
                                                        , ( 'ValidateArena1'ContractsAtVoting,          2 )
                                                        , ( 'ValidateArena1'ContractsAtBattlefield,     3 )
                                                        , ( 'Arena1'GladiatorsRevocation,                 4 )
                                                        , ( 'Arena1'GateKeeperRevocation,               5 )
                                                        , ( 'Arena1'GateKeeperDebugger,                 6 )
                                                        ]
