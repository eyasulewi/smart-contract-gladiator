
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE ImportQualifiedPost   #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}

module  GovernanceTypes.RBACTypes
            (   Assignment       (..)
            ,   Domain           (..)
            ,   Role             (..)
            )
                where

import           GHC.Generics     (Generic)
import qualified PlutusTx
import           PlutusTx.Prelude (Bool (..), Eq, (&&), (==))
import qualified Prelude          as Haskell

data Role   =   ArenaMaster
            |   Undertaker
            |   ElectionAgent
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

instance Eq Role where
    {-# INLINABLE (==) #-}
    ArenaMaster     ==  ArenaMaster   =   True
    Undertaker      ==  Undertaker    =   True
    ElectionAgent   ==  ElectionAgent =   True
    _               ==  _             =   False

PlutusTx.makeIsDataIndexed  ''Role  [ ( 'ArenaMaster,   1 )
                                    , ( 'Undertaker,    2 )
                                    , ( 'ElectionAgent, 3 )
                                    ]

data Domain =   Arena1
            |   Arena2
            |   Arena3
            |   Colosseum
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)


instance Eq Domain where
    {-# INLINABLE (==) #-}
    Arena1      ==  Arena1    =   True
    Arena2      ==  Arena2    =   True
    Arena3      ==  Arena3    =   True
    Colosseum   ==  Colosseum =   True
    _           ==  _         =   False

PlutusTx.makeIsDataIndexed  ''Domain    [ ( 'Arena1,    1 )
                                        , ( 'Arena2,    2 )
                                        , ( 'Arena3,    3 )
                                        , ( 'Colosseum, 4 )
                                        ]


data Assignment = Assignment Domain Role
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

instance Eq Assignment where
    {-# INLINABLE (==) #-}
    Assignment a b  ==  Assignment a' b' |  a == a' &&  b == b' =   True
    _               ==  _                                       =   False

PlutusTx.makeIsDataIndexed  ''Assignment    [ ( 'Assignment, 1 )]
