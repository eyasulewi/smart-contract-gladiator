
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE ImportQualifiedPost   #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}


module  GovernanceTypes.AccessControlTypes
            (   AccessControlParams     (..)
            ,   AccessControlDatum      (..)
            ,   AccessControlAction     (..)
            ,   TreasuryInfo            (..)
            ,   Arena1'DomainInfo       (..)
            ,   Arena2'DomainInfo       (..)
            ,   Arena3'DomainInfo       (..)
            ,   ColosseumDomainInfo     (..)
            )
                where

import           GHC.Generics             (Generic)
import           Plutus.V1.Ledger.Address (Address)
import           Plutus.V1.Ledger.Crypto  (PubKeyHash)
import           Plutus.V1.Ledger.Scripts (ValidatorHash)
import           Plutus.V1.Ledger.Time    (POSIXTime)
import           Plutus.V1.Ledger.Value   (CurrencySymbol)
import qualified PlutusTx
import           PlutusTx.Prelude         (Bool (..), Eq, (&&), (==))
import qualified Prelude                  as Haskell


data AccessControlParams = AccessControlParams
    {   senatePolicyID           ::  CurrencySymbol
    ,   accessControlDebuggerPKH ::  PubKeyHash
    }
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

PlutusTx.makeLift ''AccessControlParams

data TreasuryInfo
    = TreasuryInfo
        {   romanEmperorTreasuryAddress ::  Address
        ,   republicTreasuryAddress     ::  Address
        }
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

instance Eq TreasuryInfo where
    {-# INLINABLE (==) #-}
    a == b  =   ( romanEmperorTreasuryAddress   a == romanEmperorTreasuryAddress    b ) &&
                ( republicTreasuryAddress       a == republicTreasuryAddress        b )

PlutusTx.makeIsDataIndexed ''TreasuryInfo [ ( 'TreasuryInfo,    1 )]

data Arena1'DomainInfo
    = Arena1'DomainInfo
        {   arena1'RefUTxOIssuerVH ::  ValidatorHash
        ,   arena1'GateKeeperVH    ::  ValidatorHash
        ,   arena1'ProxyVH         ::  ValidatorHash
        ,   currencySymbolOfA1'DAT ::  CurrencySymbol
        ,   currencySymbolOfA1'GAT ::  CurrencySymbol
        }
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

instance Eq Arena1'DomainInfo where
    {-# INLINABLE (==) #-}
    a == b  =   ( arena1'RefUTxOIssuerVH   a == arena1'RefUTxOIssuerVH    b ) &&
                ( arena1'GateKeeperVH       a == arena1'GateKeeperVH        b ) &&
                ( arena1'ProxyVH            a == arena1'ProxyVH             b ) &&
                ( currencySymbolOfA1'DAT    a == currencySymbolOfA1'DAT     b ) &&
                ( currencySymbolOfA1'GAT    a == currencySymbolOfA1'GAT     b )

PlutusTx.makeIsDataIndexed ''Arena1'DomainInfo [ ( 'Arena1'DomainInfo,    1 )]


data Arena2'DomainInfo
    = Arena2'DomainInfo
        {   arena2'RefUTxOIssuerVH ::  ValidatorHash
        ,   arena2'GateKeeperVH    ::  ValidatorHash
        ,   arena2'ProxyVH         ::  ValidatorHash
        ,   currencySymbolOfA2'DAT ::  CurrencySymbol
        ,   currencySymbolOfA2'GAT ::  CurrencySymbol
        }
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

instance Eq Arena2'DomainInfo where
    {-# INLINABLE (==) #-}
    a == b  =   ( arena2'RefUTxOIssuerVH   a == arena2'RefUTxOIssuerVH    b ) &&
                ( arena2'GateKeeperVH       a == arena2'GateKeeperVH        b ) &&
                ( arena2'ProxyVH            a == arena2'ProxyVH             b ) &&
                ( currencySymbolOfA2'DAT    a == currencySymbolOfA2'DAT     b ) &&
                ( currencySymbolOfA2'GAT    a == currencySymbolOfA2'GAT     b )

PlutusTx.makeIsDataIndexed ''Arena2'DomainInfo [ ( 'Arena2'DomainInfo,    1 )]


data Arena3'DomainInfo
    = Arena3'DomainInfo
        {   arena3'RefUTxOIssuerVH ::  ValidatorHash
        ,   arena3'GateKeeperVH    ::  ValidatorHash
        ,   arena3'ProxyVH         ::  ValidatorHash
        ,   currencySymbolOfA3'DAT ::  CurrencySymbol
        ,   currencySymbolOfA3'GAT ::  CurrencySymbol
        }
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

instance Eq Arena3'DomainInfo where
    {-# INLINABLE (==) #-}
    a == b  =   ( arena3'RefUTxOIssuerVH   a == arena3'RefUTxOIssuerVH    b ) &&
                ( arena3'GateKeeperVH       a == arena3'GateKeeperVH        b ) &&
                ( arena3'ProxyVH            a == arena3'ProxyVH             b ) &&
                ( currencySymbolOfA3'DAT    a == currencySymbolOfA3'DAT     b ) &&
                ( currencySymbolOfA3'GAT    a == currencySymbolOfA3'GAT     b )

PlutusTx.makeIsDataIndexed ''Arena3'DomainInfo [ ( 'Arena3'DomainInfo,    1 )]

data ColosseumDomainInfo
    = ColosseumDomainInfo
        {   colosseumRefUTxOIssuerVH ::  ValidatorHash
        ,   colosseumGateKeeperVH    ::  ValidatorHash
        ,   colosseumVH              ::  ValidatorHash
        ,   currencySymbolOfCDAT     ::  CurrencySymbol
        ,   currencySymbolOfCGAT     ::  CurrencySymbol
        }
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

instance Eq ColosseumDomainInfo where
    {-# INLINABLE (==) #-}
    a == b  =   ( colosseumRefUTxOIssuerVH a == colosseumRefUTxOIssuerVH  b ) &&
                ( colosseumGateKeeperVH     a == colosseumGateKeeperVH      b ) &&
                ( colosseumVH               a == colosseumVH                b ) &&
                ( currencySymbolOfCDAT      a == currencySymbolOfCDAT       b ) &&
                ( currencySymbolOfCGAT      a == currencySymbolOfCGAT       b )

PlutusTx.makeIsDataIndexed ''ColosseumDomainInfo [ (  'ColosseumDomainInfo,    1 )]


data AccessControlDatum
    =   AccessControlRefUTxODatum
            {   treasuryInfo        :: TreasuryInfo
            ,   arena1'DomainInfo   :: Arena1'DomainInfo
            ,   arena2'DomainInfo   :: Arena2'DomainInfo
            ,   arena3'DomainInfo   :: Arena3'DomainInfo
            ,   colosseumDomainInfo :: ColosseumDomainInfo
            }
    |   GovernorDatum
            {   governorPKH     ::  PubKeyHash
            ,   governorSession ::  POSIXTime
            }
    deriving
        (Haskell.Eq, Haskell.Ord, Haskell.Show, Generic)

instance Eq AccessControlDatum where
    {-# INLINABLE (==) #-}
    AccessControlRefUTxODatum a b c d e == AccessControlRefUTxODatum a' b' c' d' e'
        |   a == a' && b == b' && c == c' && d == d' && e == e' = True
    GovernorDatum a b == GovernorDatum a' b'
        |   a == a' && b == b' = True
    _   ==  _   = False

PlutusTx.makeIsDataIndexed ''AccessControlDatum [ ( 'AccessControlRefUTxODatum, 1 )
                                                , ( 'GovernorDatum,             2 )
                                                ]


data AccessControlAction
    =   Arena1'AccessControlRefUTxOManagement
    |   Arena2'AccessControlRefUTxOManagement
    |   Arena3'AccessControlRefUTxOManagement
    |   ColosseumRefUTxOManagement
    |   DesignateGateKeeper
    |   RevokeGateKeeper
    |   RevokeGovernor
    |   RevokeAccessControlRefUTxO
    |   AccessControlDebugger
    deriving (Haskell.Show)

PlutusTx.makeIsDataIndexed ''AccessControlAction    [ ( 'Arena1'AccessControlRefUTxOManagement, 1 )
                                                    , ( 'Arena2'AccessControlRefUTxOManagement, 2 )
                                                    , ( 'Arena3'AccessControlRefUTxOManagement, 3 )
                                                    , ( 'ColosseumRefUTxOManagement,            4 )
                                                    , ( 'DesignateGateKeeper,                   5 )
                                                    , ( 'RevokeGateKeeper,                      6 )
                                                    , ( 'RevokeGovernor,                        7 )
                                                    , ( 'RevokeAccessControlRefUTxO,            8 )
                                                    , ( 'AccessControlDebugger,                 9 )
                                                    ]
