#!/bin/bash
# mixaxim@mixaxim:~$ chmod +x 6.2_createA1GateKeeperRefUTxOWithRefScript.sh; ./6.2_createA1GateKeeperRefUTxOWithRefScript.sh

# Environment Setting
. ./env.conf
reset
clear
set -e

# Logo
printf "\n"
printf "$BLUE%b" "                                                                                             .@.      "
printf "$BLUE%b" "                                                                                            .@@.      "
printf "$BLUE%b" " .#############################.            .########.                      .@@@@@@@@@@@@@@@@@.       "
printf "$BLUE%b" "  .############################.          .########.                       .@@@.           .@@@.      "
printf "$BLUE%b" "   .#######.           .#######.        .########.                        .@@@.              .@@.     "
printf "$BLUE%b" "    .#######.          .#######.      .########.                         .@@@.                .@@.    "
printf "$BLUE%b" "     .#######.         .#######.    .########.                          .@@@.                  .@@.   "
printf "$BLUE%b" "      .#######.                   .########.        .    ..    .       .@@@.                   .@@@.  "
printf "$BLUE%b" "       .#######.                .########.           .        .         .@@@.                  .@@.   "
printf "$BLUE%b" "        .#######.             .########.             ..  ::  ..          .@@@.                 .@.    "
printf "$BLUE%b" "         .#######.          .########.          .. .. .::..::  .. ..      .@@@@.                      "
printf "$BLUE%b" "          .#######.       .########.               ....::..::....          .@@@@@.          .@@.      "
printf "$CYAN%b" "          .########.    .########.           .  :  .  :  ..  :  .  :  .     .@@@@@@@@@@@@@@@@@.       "
printf "$CYAN%b" "          .#######.       .########.               ....::..::....            .@@@@@@@@@@@@@@@@@.      "
printf "$CYAN%b" "         .#######.          .########.          .. ..  ::..::. .. ..                      .@@@@@.     "
printf "$CYAN%b" "        .#######.             .########.             ..  ::  ..                             .@@@@.    "
printf "$CYAN%b" "       .#######.                .########.           .        .                               .@@@.   "
printf "$CYAN%b" "      .#######.                   .########.        .    ..    .                               .@@@.  "
printf "$CYAN%b" "     .#######.         .#######.    .########.                          .@.                     .@@@. "
printf "$CYAN%b" "    .#######.          .#######.      .########.                         .@.                   .@@@.  "
printf "$CYAN%b" "   .#######.           .#######.        .########.                        .@@.                .@@@.   "
printf "$CYAN%b" "  .############################.          .########.                       .@@.              .@@@.    "
printf "$CYAN%b" " .#############################.            .########.                      .@@@.           .@@@.     "
printf "$CYAN%b" "                                                                             .@@@@@@@@@@@@@@@@@.      "
printf "\n"
printf "$YELLOW%b" "                     E K I V A L                                             G I M B A L A B L S    "
printf "$GREEN%b" "                  https://ekival.com                                        https://gimbalabs.com    "
printf "\n"
printf "$RED%b" "<---------------------- CREATE A1GateKeeper REFERENCE UTxO WITH REFERNCE SCRIPT ---------------------->"
printf "\n"
printf "$RED%b" "<---------------------------------------------- VERSION 1 -------------------------------------------->"
printf "\n\n"

# Check Cardano Node is Running
check_process cardano-node

sleep 1

## Collateral Wallet
if [[ -f "/tmp/collat_utxo.json" ]]; then
    rm /tmp/collat_utxo.json
fi
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting Collatral Wallet UTxOs ..."
cardano-cli query utxo \
    --address "$COLLATERAL_ADDRESS" \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --out-file /tmp/collat_utxo.json
if [[ $(grep -q >/dev/null 2>&1) == $(grep '[^[:space:]]' /tmp/collat_utxo.json) && -f "/tmp/collat_utxo.json" ]]; then
    printf "\n$RED%b\n\n" "[-] ERROR: No Any UTxO Found At Collateral Address"
    exit 1
fi
addressOutputLength=$(jq length /tmp/collat_utxo.json)
if [ "$addressOutputLength" -eq "0" ]; then
    printf "\n$RED%b\n\n" "[-] ERROR: No Any UTxO Found At Collateral Address"
    exit 1
fi
collateralTxIn=$(get_address_biggest_lovelace "$COLLATERAL_ADDRESS")

sleep 1

# Get Wallets UTxOs
if [[ -f "/tmp/republic_treasury.json" ]]; then
    rm /tmp/republic_treasury.json
fi
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting Republic Treasury Address UTxOs ..."
sleep 1
cardano-cli query utxo \
    --address "$REPUBLIC_TREASURY_ADDRESS" \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --out-file /tmp/republic_treasury.json
if [[ $(grep -q >/dev/null 2>&1) == $(grep '[^[:space:]]' /tmp/republic_treasury.json) && -f "/tmp/republic_treasury.json" ]]; then
    printf "\n$RED%b\n\n" "[-] ERROR: NO Any UTxOs Found At Republic Treasury Address"
    exit
fi
sleep 1
echo ""
cardano-cli query utxo \
    --address "$REPUBLIC_TREASURY_ADDRESS" \
    --testnet-magic "$MAGIC_TESTNET_NUMBER"
echo ""
TXNS=$(jq length /tmp/republic_treasury.json)
if [ "$TXNS" -eq "0" ]; then
    printf "\n$RED%b\n\n" "[-] ERROR: NO Any UTxOs Found At Republic Treasury Address"
    exit
fi
republicTreasuryRefTxIN=$(get_address_biggest_lovelace "$REPUBLIC_TREASURY_ADDRESS")

sleep 1

# Get A1DAT Reference Script UTxO
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting A1DATMinter Reference Script UTxO ..."
if [[ $(grep -q >/dev/null 2>&1) == $(grep '[^[:space:]]' "$TX_PATH"/A1DATMinter_ref_script.signed) && -f "$TX_PATH/A1DATMinter_ref_script.signed" ]]; then
    printf "\n$RED%b\n\n" "[-] ERROR: There Is No A1DAT Reference Script UTxO"
    exit 1
fi
A1DATMinterContractRefUTxO=$(cardano-cli transaction txid --tx-file "$TX_PATH"/A1DATMinter_ref_script.signed)

sleep 1

# Get Access Control Reference Script UTxO
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting AccessControl Reference Script UTxO ..."
if [[ $(grep -q >/dev/null 2>&1) == $(grep '[^[:space:]]' "$TX_PATH"/access_control_ref_utxo_with_ref_script.signed) && -f "$TX_PATH/access_control_ref_utxo_with_ref_script.signed" ]]; then
    printf "\n$RED%b\n\n" "[-] ERROR: There Is No AccessControl Reference Script UTxO"
    exit 1
fi
accessControlRefInputUTxO=$(get_UTxO_by_token "$ACCESS_CONTROL_CONTRACT_ADDRESS" "$ACCESS_CONTROL_REF_UTxO_AUTH_TOKEN")

sleep 1

# Get A1RefUTxOIssuer Reference Script UTxO
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting A1RefUTxOIssuer Reference Script UTxO ..."
if [[ $(grep -q >/dev/null 2>&1) == $(grep '[^[:space:]]' "$TX_PATH"/A1RefUTxOIssuer_ref_script.signed) && -f "$TX_PATH/A1RefUTxOIssuer_ref_script.signed" ]]; then
    printf "\n$RED%b\n\n" "[-] ERROR: There Is No A1RefUTxOIssuer Reference Script UTxO"
    exit 1
fi
a1RefUTxOIssuerRefScriptUTxO=$(cardano-cli transaction txid --tx-file "$TX_PATH"/A1RefUTxOIssuer_ref_script.signed)
a1RefUTxOIssuerScriptActivatorUTxO=$(get_address_biggest_lovelace "$ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT_ADDRESS")

sleep 1

# Get Access Control Governor
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting Access Control Contract Address UTxOs ..."
if [[ -f "/tmp/contract_utxo.json" ]]; then
    rm /tmp/contract_utxo.json
fi
cardano-cli query utxo \
    --address "$ACCESS_CONTROL_CONTRACT_ADDRESS" \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --out-file /tmp/contract_utxo.json
sleep 1
echo ""
cardano-cli query utxo \
    --address "$ACCESS_CONTROL_CONTRACT_ADDRESS" \
    --testnet-magic "$MAGIC_TESTNET_NUMBER"
echo ""
if [[ $(grep -q >/dev/null 2>&1) == $(grep '[^[:space:]]' /tmp/contract_utxo.json) && -f "/tmp/contract_utxo.json" ]]; then
    printf "\n$RED%b\n\n" "[-] ERROR: No Any UTxO Found At Access Control Contract Address"
    exit 1
fi
addressOutputLength=$(jq length /tmp/contract_utxo.json)
if [ "$addressOutputLength" -eq "0" ]; then
    printf "\n$RED%b\n\n" "[-] ERROR: No Any UTxO Found At Access Control Contract Address"
    exit 1
fi
accessControlGovernorUTxO=$(get_UTxO_by_token "$ACCESS_CONTROL_CONTRACT_ADDRESS" "$ACCESS_CONTROL_GOVERNOR_AUTH_TOKEN")

sleep 1

# Get Minimum Amount
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting Minimum ADA Amount For Refrence Script UTxO ..."
sleep 1
cardano-cli transaction build \
    --babbage-era \
    --protocol-params-file "$WALLET_PATH"/protocol.json \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --out-file "$TX_PATH"/a1GateKeeper_ref_utxo_with_ref_script.body \
    --change-address "$REPUBLIC_TREASURY_ADDRESS" \
    --tx-in-collateral="$collateralTxIn" \
    --tx-in="$republicTreasuryRefTxIN" \
    --read-only-tx-in-reference="$accessControlRefInputUTxO" \
    --tx-in="$accessControlGovernorUTxO" \
    --tx-out-inline-datum-file "$ACCESS_CONTROL_GOVERNOR_DATUM" \
    --tx-out="$ACCESS_CONTROL_CONTRACT_ADDRESS + 2000000 + 1 $ACCESS_CONTROL_GOVERNOR_AUTH_TOKEN" \
    --spending-tx-in-reference="$accessControlRefInputUTxO" \
    --spending-plutus-script-v2 \
    --spending-reference-tx-in-inline-datum-present \
    --spending-reference-tx-in-redeemer-file "$ARENA_1_REFERENCE_UTxO_MANAGEMENT_REDEEMER" \
    --tx-in="$a1RefUTxOIssuerScriptActivatorUTxO" \
    --tx-out="$ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT_ADDRESS + 2000000" \
    --tx-out-inline-datum-file="$ARENA_1_REFERENCE_UTxO_ISSUER_SCRIPT_ACTIVATOR_UTxO_DATUM" \
    --spending-tx-in-reference="$a1RefUTxOIssuerRefScriptUTxO#0" \
    --spending-plutus-script-v2 \
    --spending-reference-tx-in-inline-datum-present \
    --spending-reference-tx-in-redeemer-file "$ISSUE_ARENA_1_GATE_KEEPER_REF_UTxO_REDEEMER" \
    --tx-out="$ARENA_1_GATE_KEEPER_CONTRACT_ADDRESS + 1000000 + 1 $ARENA_1_GATE_KEEPER_REF_UTxO_AUTH_TOKEN" \
    --tx-out-inline-datum-file "$ARENA_1_GATE_KEEPER_REF_UTxO_DATUM" \
    --tx-out-reference-script-file "$ARENA_ONE_GATE_KEEPER_CONTRACT" \
    --mint="1 $ARENA_1_GATE_KEEPER_REF_UTxO_AUTH_TOKEN" \
    --mint-tx-in-reference "$A1DATMinterContractRefUTxO#0" \
    --mint-plutus-script-v2 \
    --mint-reference-tx-in-redeemer-file "$GOVERNOR_MINT_A1DAT_REDEEMER" \
    --policy-id="${A1DAT_POLICY_ID}" \
    --required-signer-hash "$GOVERNOR_PAYMENT_PKH" \
    2>&1 | tee tmp >/dev/null 2>&1
minAmount=$(sed -n '2p' tmp)
IFS=' ' read -ra outputs <<<"$minAmount"
IFS=' ' read -ra minAmount <<<"${outputs[4]}"
echo -e "\033[1;32m[+] Minimum ADA Is Needed For Refrence Script UTxO:\033[0m" "${minAmount[0]}"
rm tmp

sleep 1

# Tx Output
a1GateKeeperTxOut="$ARENA_1_GATE_KEEPER_CONTRACT_ADDRESS + ${minAmount[0]} + 1 $ARENA_1_GATE_KEEPER_REF_UTxO_AUTH_TOKEN"
echo -e "\033[1;32m[+] Tx Output to A1GateKeeper Address: \033[0m" "$a1GateKeeperTxOut"

sleep 1

# Build Tx
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Building Tx ..."
commandOutput=$(cardano-cli transaction build \
    --babbage-era \
    --protocol-params-file "$WALLET_PATH"/protocol.json \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --out-file "$TX_PATH"/a1GateKeeper_ref_utxo_with_ref_script.body \
    --change-address "$REPUBLIC_TREASURY_ADDRESS" \
    --tx-in-collateral="$collateralTxIn" \
    --tx-in="$republicTreasuryRefTxIN" \
    --read-only-tx-in-reference="$accessControlRefInputUTxO" \
    --tx-in="$accessControlGovernorUTxO" \
    --tx-out-inline-datum-file "$ACCESS_CONTROL_GOVERNOR_DATUM" \
    --tx-out="$ACCESS_CONTROL_CONTRACT_ADDRESS + 2000000 + 1 $ACCESS_CONTROL_GOVERNOR_AUTH_TOKEN" \
    --spending-tx-in-reference="$accessControlRefInputUTxO" \
    --spending-plutus-script-v2 \
    --spending-reference-tx-in-inline-datum-present \
    --spending-reference-tx-in-redeemer-file "$ARENA_1_REFERENCE_UTxO_MANAGEMENT_REDEEMER" \
    --tx-in="$a1RefUTxOIssuerScriptActivatorUTxO" \
    --tx-out="$ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT_ADDRESS + 2000000" \
    --tx-out-inline-datum-file="$ARENA_1_REFERENCE_UTxO_ISSUER_SCRIPT_ACTIVATOR_UTxO_DATUM" \
    --spending-tx-in-reference="$a1RefUTxOIssuerRefScriptUTxO#0" \
    --spending-plutus-script-v2 \
    --spending-reference-tx-in-inline-datum-present \
    --spending-reference-tx-in-redeemer-file "$ISSUE_ARENA_1_GATE_KEEPER_REF_UTxO_REDEEMER" \
    --tx-out="$a1GateKeeperTxOut" \
    --tx-out-inline-datum-file "$ARENA_1_GATE_KEEPER_REF_UTxO_DATUM" \
    --tx-out-reference-script-file "$ARENA_ONE_GATE_KEEPER_CONTRACT" \
    --mint="1 $ARENA_1_GATE_KEEPER_REF_UTxO_AUTH_TOKEN" \
    --mint-tx-in-reference "$A1DATMinterContractRefUTxO#0" \
    --mint-plutus-script-v2 \
    --mint-reference-tx-in-redeemer-file "$GOVERNOR_MINT_A1DAT_REDEEMER" \
    --policy-id="${A1DAT_POLICY_ID}" \
    --required-signer-hash "$GOVERNOR_PAYMENT_PKH")
IFS=':' read -ra outputs <<<"$commandOutput"
IFS=' ' read -ra fee <<<"${outputs[1]}"
echo -e "\033[1;32m[+] Tx Fee:\033[0m" "${fee[1]}"

sleep 1

# Signing Tx
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Signing Tx ..."
cardano-cli transaction sign \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --tx-body-file "$TX_PATH"/a1GateKeeper_ref_utxo_with_ref_script.body \
    --out-file "$TX_PATH"/a1GateKeeper_ref_utxo_with_ref_script.signed \
    --signing-key-file "$COLLATERAL_PAYMENT_SKEY" \
    --signing-key-file "$REPUBLIC_TREASURY_PAYMENT_SKEY" \
    --signing-key-file "$GOVERNOR_PAYMENT_SKEY"

sleep 1

# Submit Tx
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Submitting Tx ..."
commandOutput=$(cardano-cli transaction submit \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --tx-file "$TX_PATH"/a1GateKeeper_ref_utxo_with_ref_script.signed)
sleep 1
echo -e "\033[1;36m[$(date +%Y-%m-%d\ %H:%M:%S)]" "${commandOutput}"

sleep 1

# Tx ID
TXID=$(cardano-cli transaction txid --tx-file "$TX_PATH"/a1GateKeeper_ref_utxo_with_ref_script.signed)
echo -e "\033[1;32m[+] Transaction ID:\033[0m" "$TXID"

sleep 1

printf "\n$RED%b\n\n" "<----------------------------------------------- DONE ------------------------------------------------>"

#
# Exit
#
