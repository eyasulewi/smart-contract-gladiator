# Colors
RED='\e[1;31m%s\e[0m'
GREEN='\e[1;32m%s\e[0m\n'
YELLOW='\e[1;33m%s\e[0m\n'
BLUE='\e[1;34m%s\e[0m\n'
MAGENTO='\e[1;35m%s\e[0m\n'
CYAN='\e[1;36m%s\e[0m\n'
WHITE='\e[1;37m%s\e[0m\n'

# Contracts Info
SENATE_CONTRACT_NAME=Senate
ACCESS_CONTROL_CONTRACT_NAME=AccessControl

ARENA_ONE_GATE_KEEPER_CONTRACT_NAME=A1GateKeeper
ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT_NAME=A1RefUTxOIssuer
ARENA_ONE_DOMAIN_AUTH_TOKEN_MINTER_CONTRACT_NAME=A1DATMinter

# Paths
SENATE_CONTRACT="../../dist/build/contracts/"${SENATE_CONTRACT_NAME}".plutus"
ACCESS_CONTROL_CONTRACT="../../dist/build/contracts/"${ACCESS_CONTROL_CONTRACT_NAME}".plutus"

ARENA_ONE_GATE_KEEPER_CONTRACT="../../dist/build/contracts/"${ARENA_ONE_GATE_KEEPER_CONTRACT_NAME}".plutus"
ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT="../../dist/build/contracts/"${ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT_NAME}".plutus"
A1DAT_MINTER_CONTRACT="../../dist/build/contracts/"${ARENA_ONE_DOMAIN_AUTH_TOKEN_MINTER_CONTRACT_NAME}".plutus"

TX_PATH=../../dist/data/transactions
DATUM_PATH=../../dist/data/datums
REDMMER_PATH=../../dist/data/redeemers
WALLET_PATH=../wallets

## Senate
DESIGNATION_REDEEMER=${REDMMER_PATH}"/"${SENATE_CONTRACT_NAME}"/designation_redeemer.json"
REVOCATION_REDEEMER=${REDMMER_PATH}"/"${SENATE_CONTRACT_NAME}"/revocation_redeemer.json"
VETO_REDEEMER=${REDMMER_PATH}"/"${SENATE_CONTRACT_NAME}"/veto_redeemer.json"
SENATE_DEBUGGER_REDEEMER=${REDMMER_PATH}"/"${SENATE_CONTRACT_NAME}"/senate_debugger_redeemer.json"

## AccessControl
ACCESS_CONTROL_REFERENCE_UTxO_DATUM=${DATUM_PATH}"/"${ACCESS_CONTROL_CONTRACT_NAME}"/access_control_ref_utxo_datum.json"
ACCESS_CONTROL_GOVERNOR_DATUM=${DATUM_PATH}"/"${ACCESS_CONTROL_CONTRACT_NAME}"/access_control_governor_datum.json"

ARENA_1_REFERENCE_UTxO_MANAGEMENT_REDEEMER=${REDMMER_PATH}"/"${ACCESS_CONTROL_CONTRACT_NAME}"/arena_1_ref_utxo_management_redeemer.json"
ARENA_2_REFERENCE_UTxO_MANAGEMENT_REDEEMER=${REDMMER_PATH}"/"${ACCESS_CONTROL_CONTRACT_NAME}"/arena_2_ref_utxo_management_redeemer.json"
ARENA_3_REFERENCE_UTxO_MANAGEMENT_REDEEMER=${REDMMER_PATH}"/"${ACCESS_CONTROL_CONTRACT_NAME}"/arena_3_ref_utxo_management_redeemer.json"
COLOSSEUM_REFERENCE_UTxO_MANAGEMENT_REDEEMER=${REDMMER_PATH}"/"${ACCESS_CONTROL_CONTRACT_NAME}"/colosseum_ref_utxo_management_redeemer.json"
DESIGNATE_GATE_KEEPER_REDEEMER=${REDMMER_PATH}"/"${ACCESS_CONTROL_CONTRACT_NAME}"/designate_gate_keeper_redeemer.json"
REVOKE_GATE_KEEPER_REDEEMER=${REDMMER_PATH}"/"${ACCESS_CONTROL_CONTRACT_NAME}"/revoke_gate_keeper_redeemer.json"
REVOKE_GOVERNOR_REDEEMER=${REDMMER_PATH}"/"${ACCESS_CONTROL_CONTRACT_NAME}"/revoke_governor_redeemer.json"
REVOKE_ACCESS_CONTROL_REFERENCE_UTxO_REDEEMER=${REDMMER_PATH}"/"${ACCESS_CONTROL_CONTRACT_NAME}"/revoke_access_control_ref_utxo_redeemer.json"
ACCESS_CONTROL_DEBUGGER_REDEEMER=${REDMMER_PATH}"/"${ACCESS_CONTROL_CONTRACT_NAME}"/access_control_debugger_redeemer.json"

## A1GateKeeper
ARENA_1_GATE_KEEPER_UNDERTAKER_ADMIN_DATUM=${DATUM_PATH}"/"${ARENA_ONE_GATE_KEEPER_CONTRACT_NAME}"/arena_1_gate_keeper_undertaker_admin_datum.json"
ARENA_1_MASTER_DATUM=${DATUM_PATH}"/"${ARENA_ONE_GATE_KEEPER_CONTRACT_NAME}"/arena_1_master_datum.json"

VALIDATE_ARENA_1_CONTRACTS_AT_MINTING_A1GAT_REDEEMER=${REDMMER_PATH}"/"${ARENA_ONE_GATE_KEEPER_CONTRACT_NAME}"/validate_arena_1_contracts_at_minting_A1GAT_redeemer.json"
VALIDATE_ARENA_1_CONTRACTS_AT_VOTING_REDEEMER=${REDMMER_PATH}"/"${ARENA_ONE_GATE_KEEPER_CONTRACT_NAME}"/validate_arena_1_contracts_at_voting_redeemer.json"
VALIDATE_ARENA_1_CONTRACTS_AT_BATTLEFIELD_REDEEMER=${REDMMER_PATH}"/"${ARENA_ONE_GATE_KEEPER_CONTRACT_NAME}"/validate_arena_1_contracts_at_battlefield_redeemer.json"
ARENA_1_CONTRACTS_REVOCATION_REDEEMER=${REDMMER_PATH}"/"${ARENA_ONE_GATE_KEEPER_CONTRACT_NAME}"/arena_1_contracts_revocation_redeemer.json"
ARENA_1_GATE_KEEPER_REDEEMER=${REDMMER_PATH}"/"${ARENA_ONE_GATE_KEEPER_CONTRACT_NAME}"/arena_1_gate_keeper_redeemer.json"

## A1RefUTxOIssuer
ARENA_1_REFERENCE_UTxO_ISSUER_SCRIPT_ACTIVATOR_UTxO_DATUM=${DATUM_PATH}"/"${ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT_NAME}"/arena_1_ref_utxo_issuer_script_activator_utxo_datum.json"
ARENA_1_GATE_KEEPER_REF_UTxO_DATUM=${DATUM_PATH}"/"${ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT_NAME}"/arena_1_gate_keeper_ref_utxo_datum.json"
ARENA_1_PROXY_REF_UTxO_DATUM=${DATUM_PATH}"/"${ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT_NAME}"/arena_1_proxy_ref_utxo_datum.json"
ARENA_1_A1GAT_MINTER_REF_UTxO_DATUM=${DATUM_PATH}"/"${ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT_NAME}"/A1GAT_minter_ref_utxo_datum.json"

ISSUE_ARENA_1_GATE_KEEPER_REF_UTxO_REDEEMER=${REDMMER_PATH}"/"${ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT_NAME}"/issue_arena_1_gate_keeper_ref_utxo_redeemer.json"
ISSUE_ARENA_1_PROXY_REF_UTxO_REDEEMER=${REDMMER_PATH}"/"${ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT_NAME}"/issue_arena_1_proxy_ref_utxo_redeemer.json"
ISSUE_A1GAT_MINTER_REF_UTxO_REDEEMER=${REDMMER_PATH}"/"${ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT_NAME}"/issue_A1GAT_minter_ref_utxo_redeemer.json"
SEND_BACK_SCRIPT_ACTIVATOR_REDEEMER=${REDMMER_PATH}"/"${ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT_NAME}"/send_back_script_activator_redeemer.json"
ARENA_1_REFERANCE_UTxO_ISSUER_DEBUGGER_REDEEMER=${REDMMER_PATH}"/"${ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT_NAME}"/arena_1_ref_utxo_issuer_debugger_redeemer.json"

## A1DATMinter
GOVERNOR_MINT_A1DAT_REDEEMER=${REDMMER_PATH}"/"${ARENA_ONE_DOMAIN_AUTH_TOKEN_MINTER_CONTRACT_NAME}"/governor_mint_A1DAT_redeemer.json"
GOVERNOR_BURN_A1DAT_REDEEMER=${REDMMER_PATH}"/"${ARENA_ONE_DOMAIN_AUTH_TOKEN_MINTER_CONTRACT_NAME}"/governor_burn_A1DAT_redeemer.json"
A1DAT_MINTER_DEBUGGER_REDEEMER=${REDMMER_PATH}"/"${ARENA_ONE_DOMAIN_AUTH_TOKEN_MINTER_CONTRACT_NAME}"/A1DAT_minter_debugger_redeemer.json"

# Variables
MAGIC_TESTNET_NUMBER=1

SENATE_POLICY_ID=$(cardano-cli transaction policyid --script-file ${SENATE_CONTRACT})
ACCESS_CONTROL_SCRIPT_HASH=$(cardano-cli transaction policyid --script-file ${ACCESS_CONTROL_CONTRACT})

ARENA_1_GATE_KEEPER_SCRIPT_HASH=$(cardano-cli transaction policyid --script-file ${ARENA_ONE_GATE_KEEPER_CONTRACT})
A1DAT_POLICY_ID=$(cardano-cli transaction policyid --script-file ${A1DAT_MINTER_CONTRACT})

# Addresses
SENATE_CONTRACT_ADDRESS=$(cardano-cli address build --payment-script-file ${SENATE_CONTRACT} --testnet-magic ${MAGIC_TESTNET_NUMBER})
ACCESS_CONTROL_CONTRACT_ADDRESS=$(cardano-cli address build --payment-script-file ${ACCESS_CONTROL_CONTRACT} --testnet-magic ${MAGIC_TESTNET_NUMBER})

ARENA_1_GATE_KEEPER_CONTRACT_ADDRESS=$(cardano-cli address build --payment-script-file ${ARENA_ONE_GATE_KEEPER_CONTRACT} --testnet-magic ${MAGIC_TESTNET_NUMBER})
ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT_ADDRESS=$(cardano-cli address build --payment-script-file ${ARENA_ONE_REFERENCE_UTxO_ISSUER_CONTRACT} --testnet-magic ${MAGIC_TESTNET_NUMBER})
A1DAT_MINTER_CONTRACT_ADDRESS=$(cardano-cli address build --payment-script-file ${A1DAT_MINTER_CONTRACT} --testnet-magic ${MAGIC_TESTNET_NUMBER})

COLLATERAL_ADDRESS=$(cat ${WALLET_PATH}/collateral/collateral.1.0.TestnetDelegation.addr)
DEBUGGER_ADDRESS=$(cat ${WALLET_PATH}/debugger/debugger.1.0.TestnetDelegation.addr)

REPUBLIC_TREASURY_ADDRESS=$(cat ${WALLET_PATH}/republic_treasury/republic_treasury.1.0.TestnetDelegation.addr)
EMPEROR_TREASURY_ADDRESS=$(cat ${WALLET_PATH}/emperor_treasury/emperor_treasury.1.0.TestnetDelegation.addr)

CAESAR_ADDRESS=$(cat ${WALLET_PATH}/caesar/caesar.1.0.TestnetDelegation.addr)
GOVERNOR_ADDRESS=$(cat ${WALLET_PATH}/governor/governor.1.0.TestnetDelegation.addr)

SENATOR_1_ADDRESS=$(cat ${WALLET_PATH}/senate/senator1/senator1.1.0.TestnetDelegation.addr)
SENATOR_2_ADDRESS=$(cat ${WALLET_PATH}/senate/senator2/senator2.1.0.TestnetDelegation.addr)
SENATOR_3_ADDRESS=$(cat ${WALLET_PATH}/senate/senator3/senator3.1.0.TestnetDelegation.addr)
SENATOR_4_ADDRESS=$(cat ${WALLET_PATH}/senate/senator4/senator4.1.0.TestnetDelegation.addr)
SENATOR_5_ADDRESS=$(cat ${WALLET_PATH}/senate/senator5/senator5.1.0.TestnetDelegation.addr)
SENATOR_6_ADDRESS=$(cat ${WALLET_PATH}/senate/senator6/senator6.1.0.TestnetDelegation.addr)
SENATOR_7_ADDRESS=$(cat ${WALLET_PATH}/senate/senator7/senator7.1.0.TestnetDelegation.addr)

SPECTOR_1_ADDRESS=$(cat ${WALLET_PATH}/spectors/spector1/spector1.1.0.TestnetDelegation.addr)
SPECTOR_2_ADDRESS=$(cat ${WALLET_PATH}/spectors/spector2/spector2.1.0.TestnetDelegation.addr)
SPECTOR_3_ADDRESS=$(cat ${WALLET_PATH}/spectors/spector3/spector3.1.0.TestnetDelegation.addr)
SPECTOR_4_ADDRESS=$(cat ${WALLET_PATH}/spectors/spector4/spector4.1.0.TestnetDelegation.addr)
SPECTOR_5_ADDRESS=$(cat ${WALLET_PATH}/spectors/spector5/spector5.1.0.TestnetDelegation.addr)
SPECTOR_6_ADDRESS=$(cat ${WALLET_PATH}/spectors/spector6/spector6.1.0.TestnetDelegation.addr)
SPECTOR_7_ADDRESS=$(cat ${WALLET_PATH}/spectors/spector7/spector7.1.0.TestnetDelegation.addr)
SPECTOR_8_ADDRESS=$(cat ${WALLET_PATH}/spectors/spector8/spector8.1.0.TestnetDelegation.addr)
SPECTOR_9_ADDRESS=$(cat ${WALLET_PATH}/spectors/spector9/spector9.1.0.TestnetDelegation.addr)
SPECTOR_10_ADDRESS=$(cat ${WALLET_PATH}/spectors/spector10/spector10.1.0.TestnetDelegation.addr)
SPECTOR_11_ADDRESS=$(cat ${WALLET_PATH}/spectors/spector11/spector11.1.0.TestnetDelegation.addr)
SPECTOR_12_ADDRESS=$(cat ${WALLET_PATH}/spectors/spector12/spector12.1.0.TestnetDelegation.addr)

ARENA_1_MASTER_ADDRESS=$(cat ${WALLET_PATH}/arena_one_master/arena_one_master.1.0.TestnetDelegation.addr)
ARENA_1_UNDERTAKER_ADDRESS=$(cat ${WALLET_PATH}/arena_one_undertaker/arena_one_undertaker.1.0.TestnetDelegation.addr)

# Payment PKH
COLLATERAL_PAYMENT_PKH=$(cat ${WALLET_PATH}/collateral/collateral.1.Payment.pkh)
DEBUGGER_PAYMENT_PKH=$(cat ${WALLET_PATH}/debugger/debugger.1.Payment.pkh)

REPUBLIC_TREASURY_PAYMENT_PKH=$(cat ${WALLET_PATH}/republic_treasury/republic_treasury.1.Payment.pkh)
EMPEROR_TREASURY_PAYMENT_PKH=$(cat ${WALLET_PATH}/emperor_treasury/emperor_treasury.1.Payment.pkh)

CAESAR_PAYMENT_PKH=$(cat ${WALLET_PATH}/caesar/caesar.1.Payment.pkh)
GOVERNOR_PAYMENT_PKH=$(cat ${WALLET_PATH}/governor/governor.1.Payment.pkh)

SENATOR_1_PAYMENT_PKH=$(cat ${WALLET_PATH}/senate/senator1/senator1.1.Payment.pkh)
SENATOR_2_PAYMENT_PKH=$(cat ${WALLET_PATH}/senate/senator2/senator2.1.Payment.pkh)
SENATOR_3_PAYMENT_PKH=$(cat ${WALLET_PATH}/senate/senator3/senator3.1.Payment.pkh)
SENATOR_4_PAYMENT_PKH=$(cat ${WALLET_PATH}/senate/senator4/senator4.1.Payment.pkh)
SENATOR_5_PAYMENT_PKH=$(cat ${WALLET_PATH}/senate/senator5/senator5.1.Payment.pkh)
SENATOR_6_PAYMENT_PKH=$(cat ${WALLET_PATH}/senate/senator6/senator6.1.Payment.pkh)
SENATOR_7_PAYMENT_PKH=$(cat ${WALLET_PATH}/senate/senator7/senator7.1.Payment.pkh)

SPECTOR_1_PAYMENT_PKH=$(cat ${WALLET_PATH}/spectors/spector1/spector1.1.Payment.pkh)
SPECTOR_2_PAYMENT_PKH=$(cat ${WALLET_PATH}/spectors/spector2/spector2.1.Payment.pkh)
SPECTOR_3_PAYMENT_PKH=$(cat ${WALLET_PATH}/spectors/spector3/spector3.1.Payment.pkh)
SPECTOR_4_PAYMENT_PKH=$(cat ${WALLET_PATH}/spectors/spector4/spector4.1.Payment.pkh)
SPECTOR_5_PAYMENT_PKH=$(cat ${WALLET_PATH}/spectors/spector5/spector5.1.Payment.pkh)
SPECTOR_6_PAYMENT_PKH=$(cat ${WALLET_PATH}/spectors/spector6/spector6.1.Payment.pkh)
SPECTOR_7_PAYMENT_PKH=$(cat ${WALLET_PATH}/spectors/spector7/spector7.1.Payment.pkh)
SPECTOR_8_PAYMENT_PKH=$(cat ${WALLET_PATH}/spectors/spector8/spector8.1.Payment.pkh)
SPECTOR_9_PAYMENT_PKH=$(cat ${WALLET_PATH}/spectors/spector9/spector9.1.Payment.pkh)
SPECTOR_10_PAYMENT_PKH=$(cat ${WALLET_PATH}/spectors/spector10/spector10.1.Payment.pkh)
SPECTOR_11_PAYMENT_PKH=$(cat ${WALLET_PATH}/spectors/spector11/spector11.1.Payment.pkh)
SPECTOR_12_PAYMENT_PKH=$(cat ${WALLET_PATH}/spectors/spector12/spector12.1.Payment.pkh)

ARENA_1_MASTER_PAYMENT_PKH=$(cat ${WALLET_PATH}/arena_one_master/arena_one_master.1.Payment.pkh)
ARENA_1_UNDERTAKER_PAYMENT_PKH=$(cat ${WALLET_PATH}/arena_one_undertaker/arena_one_undertaker.1.Payment.pkh)

# Payment Singing Key
COLLATERAL_PAYMENT_SKEY=${WALLET_PATH}/collateral/collateral.1.Payment.xsk
DEBUGGER_PAYMENT_SKEY=${WALLET_PATH}/debugger/debugger.1.Payment.xsk

REPUBLIC_TREASURY_PAYMENT_SKEY=${WALLET_PATH}/republic_treasury/republic_treasury.1.Payment.xsk
EMPEROR_TREASURY_PAYMENT_SKEY=${WALLET_PATH}/emperor_treasury/emperor_treasury.1.Payment.xsk

CAESAR_PAYMENT_SKEY=${WALLET_PATH}/caesar/caesar.1.Payment.xsk
GOVERNOR_PAYMENT_SKEY=${WALLET_PATH}/governor/governor.1.Payment.xsk

SENATOR_1_PAYMENT_SKEY=${WALLET_PATH}/senate/senator1/senator1.1.Payment.xsk
SENATOR_2_PAYMENT_SKEY=${WALLET_PATH}/senate/senator2/senator2.1.Payment.xsk
SENATOR_3_PAYMENT_SKEY=${WALLET_PATH}/senate/senator3/senator3.1.Payment.xsk
SENATOR_4_PAYMENT_SKEY=${WALLET_PATH}/senate/senator4/senator4.1.Payment.xsk
SENATOR_5_PAYMENT_SKEY=${WALLET_PATH}/senate/senator5/senator5.1.Payment.xsk
SENATOR_6_PAYMENT_SKEY=${WALLET_PATH}/senate/senator6/senator6.1.Payment.xsk
SENATOR_7_PAYMENT_SKEY=${WALLET_PATH}/senate/senator7/senator7.1.Payment.xsk

SPECTOR_1_PAYMENT_SKEY=${WALLET_PATH}/spectors/spector1/spector1.1.Payment.xsk
SPECTOR_2_PAYMENT_SKEY=${WALLET_PATH}/spectors/spector2/spector2.1.Payment.xsk
SPECTOR_3_PAYMENT_SKEY=${WALLET_PATH}/spectors/spector3/spector3.1.Payment.xsk
SPECTOR_4_PAYMENT_SKEY=${WALLET_PATH}/spectors/spector4/spector4.1.Payment.xsk
SPECTOR_5_PAYMENT_SKEY=${WALLET_PATH}/spectors/spector5/spector5.1.Payment.xsk
SPECTOR_6_PAYMENT_SKEY=${WALLET_PATH}/spectors/spector6/spector6.1.Payment.xsk
SPECTOR_7_PAYMENT_SKEY=${WALLET_PATH}/spectors/spector7/spector7.1.Payment.xsk
SPECTOR_8_PAYMENT_SKEY=${WALLET_PATH}/spectors/spector8/spector8.1.Payment.xsk
SPECTOR_9_PAYMENT_SKEY=${WALLET_PATH}/spectors/spector9/spector9.1.Payment.xsk
SPECTOR_10_PAYMENT_SKEY=${WALLET_PATH}/spectors/spector10/spector10.1.Payment.xsk
SPECTOR_11_PAYMENT_SKEY=${WALLET_PATH}/spectors/spector11/spector11.1.Payment.xsk
SPECTOR_12_PAYMENT_SKEY=${WALLET_PATH}/spectors/spector12/spector12.1.Payment.xsk

ARENA_1_MASTER_PAYMENT_SKEY=${WALLET_PATH}/arena_one_master/arena_one_master.1.Payment.xsk
ARENA_1_UNDERTAKER_PAYMENT_SKEY=${WALLET_PATH}/arena_one_undertaker/arena_one_undertaker.1.Payment.xsk

# Payment Verification Key
COLLATERAL_PAYMENT_VKEY=${WALLET_PATH}/collateral/collateral.1.Payment.xvk
DEBUGGER_PAYMENT_VKEY=${WALLET_PATH}/debugger/debugger.1.Payment.xvk

REPUBLIC_TREASURY_PAYMENT_VKEY=${WALLET_PATH}/republic_treasury/republic_treasury.1.Payment.xvk
EMPEROR_TREASURY_PAYMENT_VKEY=${WALLET_PATH}/emperor_treasury/emperor_treasury.1.Payment.xvk

CAESAR_PAYMENT_VKEY=${WALLET_PATH}/caesar/caesar.1.Payment.xvk
GOVERNOR_PAYMENT_VKEY=${WALLET_PATH}/governor/governor.1.Payment.xvk

SENATOR_1_PAYMENT_VKEY=${WALLET_PATH}/senate/senator1/senator1.1.Payment.xvk
SENATOR_2_PAYMENT_VKEY=${WALLET_PATH}/senate/senator2/senator2.1.Payment.xvk
SENATOR_3_PAYMENT_VKEY=${WALLET_PATH}/senate/senator3/senator3.1.Payment.xvk
SENATOR_4_PAYMENT_VKEY=${WALLET_PATH}/senate/senator4/senator4.1.Payment.xvk
SENATOR_5_PAYMENT_VKEY=${WALLET_PATH}/senate/senator5/senator5.1.Payment.xvk
SENATOR_6_PAYMENT_VKEY=${WALLET_PATH}/senate/senator6/senator6.1.Payment.xvk
SENATOR_7_PAYMENT_VKEY=${WALLET_PATH}/senate/senator7/senator7.1.Payment.xvk

SPECTOR_1_PAYMENT_VKEY=${WALLET_PATH}/spectors/spector1/spector1.1.Payment.xvk
SPECTOR_2_PAYMENT_VKEY=${WALLET_PATH}/spectors/spector2/spector2.1.Payment.xvk
SPECTOR_3_PAYMENT_VKEY=${WALLET_PATH}/spectors/spector3/spector3.1.Payment.xvk
SPECTOR_4_PAYMENT_VKEY=${WALLET_PATH}/spectors/spector4/spector4.1.Payment.xvk
SPECTOR_5_PAYMENT_VKEY=${WALLET_PATH}/spectors/spector5/spector5.1.Payment.xvk
SPECTOR_6_PAYMENT_VKEY=${WALLET_PATH}/spectors/spector6/spector6.1.Payment.xvk
SPECTOR_7_PAYMENT_VKEY=${WALLET_PATH}/spectors/spector7/spector7.1.Payment.xvk
SPECTOR_8_PAYMENT_VKEY=${WALLET_PATH}/spectors/spector8/spector8.1.Payment.xvk
SPECTOR_9_PAYMENT_VKEY=${WALLET_PATH}/spectors/spector9/spector9.1.Payment.xvk
SPECTOR_10_PAYMENT_VKEY=${WALLET_PATH}/spectors/spector10/spector10.1.Payment.xvk
SPECTOR_11_PAYMENT_VKEY=${WALLET_PATH}/spectors/spector11/spector11.1.Payment.xvk
SPECTOR_12_PAYMENT_VKEY=${WALLET_PATH}/spectors/spector12/spector12.1.Payment.xvk

ARENA_1_MASTER_PAYMENT_VKEY=${WALLET_PATH}/arena_one_master/arena_one_master.1.Payment.xvk
ARENA_1_UNDERTAKER_PAYMENT_VKEY=${WALLET_PATH}/arena_one_undertaker/arena_one_undertaker.1.Payment.xvk

# Access Control Refrence UTxO Auth Token
ACCESS_CONTROL_REF_UTxO_AUTH_TOKEN=${SENATE_POLICY_ID}.${ACCESS_CONTROL_SCRIPT_HASH}

# ARENA 1 Gate Keeper Refrence UTxO Auth Token
ARENA_1_GATE_KEEPER_REF_UTxO_AUTH_TOKEN=${A1DAT_POLICY_ID}.${ARENA_1_GATE_KEEPER_SCRIPT_HASH}

# Access Control Governor Auth Token
## 318642c25f4d2dc3c59e60e3867991c8b9bed2cced13b5d2a127523e
## 3331383634326332356634643264633363353965363065333836373939316338623962656432636365643133623564326131323735323365
ACCESS_CONTROL_GOVERNOR_AUTH_TOKEN=${SENATE_POLICY_ID}.${GOVERNOR_PAYMENT_PKH}

# Arena One Master Auth Token
## 31ee873ae53622e1d4fa6d225475c923472487ca6b85823265c29b88
## 3331656538373361653533363232653164346661366432323534373563393233343732343837636136623835383233323635633239623838
ARENA_1_MASTER_AUTH_TOKEN=${A1DAT_POLICY_ID}.${ARENA_1_MASTER_PAYMENT_PKH}

# Arena One Undertaker Auth Token
## cdcc6941d223c4b6313e589d041d7ae658a8400271dc36a80f4c1d36
## 6364636336393431643232336334623633313365353839643034316437616536353861383430303237316463333661383066346331643336
ARENA_1_UNDERTAKER_AUTH_TOKEN=${A1DAT_POLICY_ID}.${ARENA_1_UNDERTAKER_PAYMENT_PKH}

# Helper Functions

if [ ! -d ${TX_PATH} ]; then
    mkdir -p ${TX_PATH}
fi

if [ ! -d ${DATUM_PATH} ]; then
    mkdir -p ${DATUM_PATH}
fi

if [ ! -d ${REDMMER_PATH} ]; then
    mkdir -p ${REDMMER_PATH}
fi

if [ ! -d ${WALLET_PATH} ]; then
    mkdir -p ${WALLET_PATH}
fi

check_process() {
    printf "$CYAN" "[$(date +%Y-%m-%d\ %H:%M:%S)] Checking If Process '$1' Exists..."
    [ "$1" = "" ] && return 0
    PROCESS_NUM=$(ps -ef | grep "$1" | grep -v "grep" | wc -l)
    if [ $PROCESS_NUM -eq 0 ]; then
        printf "\n$RED\n" "[-] ERROR: '$1' Is Not Running"
        echo ""
        exit
    fi
}

# # retrieve the amount of lovelace in the biggest (in amount of lovelace had) tx
# # $1 - address to lookup
get_address_biggest_lovelace() {
    cardano-cli query utxo --address $1 --testnet-magic ${MAGIC_TESTNET_NUMBER} |
        tail -n +3 |
        awk '{printf "%s#%s %s \n", $1 , $2, $3}' |
        sort -rn -k2 |
        head -n1 |
        awk '{print $1}'
}

# $1 is the address
# $2 is the token
get_UTxO_by_token() {
    for i in {1..10}; do
        utxoAttachedHex=$(
            cardano-cli query utxo --address $1 --testnet-magic ${MAGIC_TESTNET_NUMBER} |
                tail -n +3 |
                awk '{printf "%s %s#%s %s\n",$6, $1, $2, $7}' |
                sort -gr |
                awk -v i="$i" 'NR == i {printf("%s", $3)}'
        )
        if [ "$utxoAttachedHex" == "$2" ]; then
            cardano-cli query utxo --address $1 --testnet-magic ${MAGIC_TESTNET_NUMBER} |
                tail -n +3 |
                awk '{printf "%s %s#%s\n",$6, $1, $2}' |
                sort -gr |
                awk -v i="$i" 'NR == i {printf("%s", $2)}'
            return
        fi
    done
}

# $1 is the address
# $2 is the UtxO
get_UTxO_lovelace_amount() {
    for i in {1..10}; do
        utxoAttachedHex=$(
            cardano-cli query utxo --address $1 --testnet-magic ${MAGIC_TESTNET_NUMBER} |
                tail -n +3 |
                awk '{printf "%s#%s %s\n",$1, $2, $3}' |
                sort -gr |
                awk -v i="$i" 'NR == i {printf("%s", $1)}'
        )
        if [ "$utxoAttachedHex" == "$2" ]; then
            cardano-cli query utxo --address $1 --testnet-magic ${MAGIC_TESTNET_NUMBER} |
                tail -n +3 |
                awk '{printf "%s#%s %s\n",$1, $2, $3}' |
                sort -gr |
                awk -v i="$i" 'NR == i {printf("%s", $2)}'
            return
        fi
    done
}

# cardano-cli query utxo --address addr_test1wpv3tyzrrl8jmh47lrrnea6yw3khn66cs2zqrrz2acga9hs0fqtz3 $MAGIC |
#     tail -n +3 |
#     awk '{printf "%s#%s %s\n"$1, $2, $3}' |
#     sort -rn -k3 |
#     awk -v i="1" 'NR == i {printf("%s", $1)}'

# cardano-cli query utxo --address addr_test1qztn43dtwkaz9lsawm0drrtmc4m0u7qtcmks4lqcqktgp54rqghlrj99l5vrdmyrtg6mhkyxa88kwq5yf225a4m9pkes79dh8s $MAGIC |
#     tail -n +3 |
#     awk '{printf "%s %s#%s %s\n",$6, $1, $2, $7}' |
#     sort -gr -k2

# $1 is address
# $2 is the token
get_token_count() {
    for i in {1..10}; do
        utxoAttachedHex=$(
            cardano-cli query utxo --address $1 --testnet-magic ${MAGIC_TESTNET_NUMBER} |
                tail -n +3 |
                awk '{printf "%s %s#%s %s\n",$6, $1, $2, $7}' |
                sort -gr |
                awk -v i="$i" 'NR == i {printf("%s", $3)}'
        )
        if [ "$utxoAttachedHex" == "$2" ]; then
            cardano-cli query utxo --address $1 --testnet-magic ${MAGIC_TESTNET_NUMBER} |
                tail -n +3 |
                awk '{printf "%s %s#%s %s\n",$6, $1, $2, $7}' |
                sort -gr |
                awk -v i="$i" 'NR == i {printf("%s", $1)}'
            return
        fi
    done
}

# $1 is contract address
# $2 is the amount
get_contract_UTxO() {
    for i in {1..20}; do
        utxoAttachedHex=$(
            cardano-cli query utxo --address $1 --testnet-magic ${MAGIC_TESTNET_NUMBER} |
                tail -n +3 |
                awk '{printf "%s %s\n", $1, $6}' |
                sort -gr |
                awk -v i="$i" 'NR == i {printf("%s", $2)}'
        )
        if [ "$utxoAttachedHex" == "$2" ]; then
            cardano-cli query utxo --address $1 --testnet-magic ${MAGIC_TESTNET_NUMBER} |
                tail -n +3 |
                awk '{printf "%s#%s\n",$1, $2}' |
                sort -gr |
                awk -v i="$i" 'NR == i {printf("%s", $1)}'
            return
        fi
    done
}

# $1 is order/offer amount
get_current_slot_number() {
    cardano-cli query tip --testnet-magic ${MAGIC_TESTNET_NUMBER} |
        awk -v i="6" 'NR == i {printf("%s", $2)}' |
        cut -d ',' -f 1
}
