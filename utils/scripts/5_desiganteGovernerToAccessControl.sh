#!/bin/bash
# mixaxim@mixaxim:~$ chmod +x 5_desiganteGovernerToAccessControl.sh; ./5_desiganteGovernerToAccessControl.sh

# Environment Setting
. ./env.conf
reset
clear
set -e

# Logo
printf "\n"
printf "$BLUE%b" "                                                                                             .@.      "
printf "$BLUE%b" "                                                                                            .@@.      "
printf "$BLUE%b" " .#############################.            .########.                      .@@@@@@@@@@@@@@@@@.       "
printf "$BLUE%b" "  .############################.          .########.                       .@@@.           .@@@.      "
printf "$BLUE%b" "   .#######.           .#######.        .########.                        .@@@.              .@@.     "
printf "$BLUE%b" "    .#######.          .#######.      .########.                         .@@@.                .@@.    "
printf "$BLUE%b" "     .#######.         .#######.    .########.                          .@@@.                  .@@.   "
printf "$BLUE%b" "      .#######.                   .########.        .    ..    .       .@@@.                   .@@@.  "
printf "$BLUE%b" "       .#######.                .########.           .        .         .@@@.                  .@@.   "
printf "$BLUE%b" "        .#######.             .########.             ..  ::  ..          .@@@.                 .@.    "
printf "$BLUE%b" "         .#######.          .########.          .. .. .::..::  .. ..      .@@@@.                      "
printf "$BLUE%b" "          .#######.       .########.               ....::..::....          .@@@@@.          .@@.      "
printf "$CYAN%b" "          .########.    .########.           .  :  .  :  ..  :  .  :  .     .@@@@@@@@@@@@@@@@@.       "
printf "$CYAN%b" "          .#######.       .########.               ....::..::....            .@@@@@@@@@@@@@@@@@.      "
printf "$CYAN%b" "         .#######.          .########.          .. ..  ::..::. .. ..                      .@@@@@.     "
printf "$CYAN%b" "        .#######.             .########.             ..  ::  ..                             .@@@@.    "
printf "$CYAN%b" "       .#######.                .########.           .        .                               .@@@.   "
printf "$CYAN%b" "      .#######.                   .########.        .    ..    .                               .@@@.  "
printf "$CYAN%b" "     .#######.         .#######.    .########.                          .@.                     .@@@. "
printf "$CYAN%b" "    .#######.          .#######.      .########.                         .@.                   .@@@.  "
printf "$CYAN%b" "   .#######.           .#######.        .########.                        .@@.                .@@@.   "
printf "$CYAN%b" "  .############################.          .########.                       .@@.              .@@@.    "
printf "$CYAN%b" " .#############################.            .########.                      .@@@.           .@@@.     "
printf "$CYAN%b" "                                                                             .@@@@@@@@@@@@@@@@@.      "
printf "\n"
printf "$YELLOW%b" "                     E K I V A L                                             G I M B A L A B L S    "
printf "$GREEN%b" "                  https://ekival.com                                        https://gimbalabs.com    "
printf "\n"
printf "$RED%b" "<------------------------------- DESIGNATE GOVERNER TO ACCESS CONTROL -------------------------------->"
printf "\n\n"

# Check Cardano Node is Running
check_process cardano-node

sleep 1

## Collateral Wallet
if [[ -f "/tmp/collat_utxo.json" ]]; then
    rm /tmp/collat_utxo.json
fi
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting Collatral Wallet UTxOs ..."
cardano-cli query utxo \
    --address "$COLLATERAL_ADDRESS" \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --out-file /tmp/collat_utxo.json
if [[ $(grep -q >/dev/null 2>&1) == $(grep '[^[:space:]]' /tmp/collat_utxo.json) && -f "/tmp/collat_utxo.json" ]]; then
    printf "\n$RED%b\n\n" "[-] ERROR: No Any UTxO Found At Collateral Address"
    exit 1
fi
addressOutputLength=$(jq length /tmp/collat_utxo.json)
if [ "$addressOutputLength" -eq "0" ]; then
    printf "\n$RED%b\n\n" "[-] ERROR: No Any UTxO Found At Collateral Address"
    exit 1
fi
collateralTxIn=$(get_address_biggest_lovelace "$COLLATERAL_ADDRESS")

sleep 1

# Get Wallets UTxOs
if [[ -f "/tmp/republic_treasury.json" ]]; then
    rm /tmp/republic_treasury.json
fi
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting Republic Treasury Address UTxOs ..."
sleep 1
cardano-cli query utxo \
    --address "$REPUBLIC_TREASURY_ADDRESS" \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --out-file /tmp/republic_treasury.json
if [[ $(grep -q >/dev/null 2>&1) == $(grep '[^[:space:]]' /tmp/republic_treasury.json) && -f "/tmp/republic_treasury.json" ]]; then
    printf "\n$RED%b\n\n" "[-] ERROR: NO Any UTxOs Found At Republic Treasury Address"
    exit
fi
echo ""
cardano-cli query utxo \
    --address "$REPUBLIC_TREASURY_ADDRESS" \
    --testnet-magic "$MAGIC_TESTNET_NUMBER"
echo ""
TXNS=$(jq length /tmp/republic_treasury.json)
if [ "$TXNS" -eq "0" ]; then
    printf "\n$RED%b\n\n" "[-] ERROR: NO Any UTxOs Found At Republic Treasury Address"
    exit
fi
republicTreasuryRefTxIN=$(get_address_biggest_lovelace "$REPUBLIC_TREASURY_ADDRESS")

sleep 1

SenateRefScriptUTxO=$(cardano-cli transaction txid --tx-file "$TX_PATH"/senate_ref_script.signed)

# Tx Output
accessControlTxOut="$ACCESS_CONTROL_CONTRACT_ADDRESS + 2000000 + 1 $ACCESS_CONTROL_GOVERNOR_AUTH_TOKEN"
echo -e "\033[1;32m[+] Tx Output to Access Control Address: \033[0m" "$accessControlTxOut"

sleep 1

# Build Tx
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Building Tx ..."
commandOutput=$(cardano-cli transaction build \
    --babbage-era \
    --protocol-params-file "$WALLET_PATH"/protocol.json \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --out-file "$TX_PATH"/designate_governor_to_access_control.body \
    --change-address "$REPUBLIC_TREASURY_ADDRESS" \
    --tx-in-collateral="$collateralTxIn" \
    --tx-in "${republicTreasuryRefTxIN}" \
    --tx-out="$accessControlTxOut" \
    --tx-out-inline-datum-file "$ACCESS_CONTROL_GOVERNOR_DATUM" \
    --mint="1 $ACCESS_CONTROL_GOVERNOR_AUTH_TOKEN" \
    --mint-tx-in-reference "$SenateRefScriptUTxO#0" \
    --mint-plutus-script-v2 \
    --mint-reference-tx-in-redeemer-file "$DESIGNATION_REDEEMER" \
    --policy-id="${SENATE_POLICY_ID}" \
    --required-signer-hash "$SENATOR_1_PAYMENT_PKH" \
    --required-signer-hash "$SENATOR_2_PAYMENT_PKH" \
    --required-signer-hash "$SENATOR_3_PAYMENT_PKH" \
    --required-signer-hash "$SENATOR_4_PAYMENT_PKH" \
    --required-signer-hash "$SENATOR_5_PAYMENT_PKH" \
    --required-signer-hash "$SENATOR_6_PAYMENT_PKH" \
    --required-signer-hash "$SENATOR_7_PAYMENT_PKH" \
    --required-signer-hash "$CAESAR_PAYMENT_PKH")
IFS=':' read -ra outputs <<<"$commandOutput"
IFS=' ' read -ra fee <<<"${outputs[1]}"
echo -e "\033[1;32m[+] Tx Fee:\033[0m" "${fee[1]}"

sleep 1

# Signing Tx
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Signing Tx ..."
cardano-cli transaction sign \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --tx-body-file "$TX_PATH"/designate_governor_to_access_control.body \
    --out-file "$TX_PATH"/designate_governor_to_access_control.signed \
    --signing-key-file "$COLLATERAL_PAYMENT_SKEY" \
    --signing-key-file "$REPUBLIC_TREASURY_PAYMENT_SKEY" \
    --signing-key-file "$SENATOR_1_PAYMENT_SKEY" \
    --signing-key-file "$SENATOR_2_PAYMENT_SKEY" \
    --signing-key-file "$SENATOR_3_PAYMENT_SKEY" \
    --signing-key-file "$SENATOR_4_PAYMENT_SKEY" \
    --signing-key-file "$SENATOR_5_PAYMENT_SKEY" \
    --signing-key-file "$SENATOR_6_PAYMENT_SKEY" \
    --signing-key-file "$SENATOR_7_PAYMENT_SKEY" \
    --signing-key-file "$CAESAR_PAYMENT_SKEY"

sleep 1

# Submit Tx
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Submitting Tx ..."
commandOutput=$(cardano-cli transaction submit \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --tx-file "$TX_PATH"/designate_governor_to_access_control.signed)
sleep 1
echo -e "\033[1;36m[$(date +%Y-%m-%d\ %H:%M:%S)]" "${commandOutput}"

sleep 1

# Tx ID
TXID=$(cardano-cli transaction txid --tx-file "$TX_PATH"/designate_governor_to_access_control.signed)
echo -e "\033[1;32m[+] Transaction ID:\033[0m" "$TXID"

sleep 1

printf "\n$RED%b\n\n" "<----------------------------------------------- DONE ------------------------------------------------>"

#
# Exit
#
