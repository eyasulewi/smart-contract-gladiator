#!/bin/bash
# mixaxim@mixaxim:~$ chmod +x 1_createSenateRefScriptUTxO.sh; ./1_createSenateRefScriptUTxO.sh

# Environment Setting
. ./env.conf
reset
clear
set -e

# Logo
printf "\n"
printf "$BLUE%b" "                                                                                             .@.      "
printf "$BLUE%b" "                                                                                            .@@.      "
printf "$BLUE%b" " .#############################.            .########.                      .@@@@@@@@@@@@@@@@@.       "
printf "$BLUE%b" "  .############################.          .########.                       .@@@.           .@@@.      "
printf "$BLUE%b" "   .#######.           .#######.        .########.                        .@@@.              .@@.     "
printf "$BLUE%b" "    .#######.          .#######.      .########.                         .@@@.                .@@.    "
printf "$BLUE%b" "     .#######.         .#######.    .########.                          .@@@.                  .@@.   "
printf "$BLUE%b" "      .#######.                   .########.        .    ..    .       .@@@.                   .@@@.  "
printf "$BLUE%b" "       .#######.                .########.           .        .         .@@@.                  .@@.   "
printf "$BLUE%b" "        .#######.             .########.             ..  ::  ..          .@@@.                 .@.    "
printf "$BLUE%b" "         .#######.          .########.          .. .. .::..::  .. ..      .@@@@.                      "
printf "$BLUE%b" "          .#######.       .########.               ....::..::....          .@@@@@.          .@@.      "
printf "$CYAN%b" "          .########.    .########.           .  :  .  :  ..  :  .  :  .     .@@@@@@@@@@@@@@@@@.       "
printf "$CYAN%b" "          .#######.       .########.               ....::..::....            .@@@@@@@@@@@@@@@@@.      "
printf "$CYAN%b" "         .#######.          .########.          .. ..  ::..::. .. ..                      .@@@@@.     "
printf "$CYAN%b" "        .#######.             .########.             ..  ::  ..                             .@@@@.    "
printf "$CYAN%b" "       .#######.                .########.           .        .                               .@@@.   "
printf "$CYAN%b" "      .#######.                   .########.        .    ..    .                               .@@@.  "
printf "$CYAN%b" "     .#######.         .#######.    .########.                          .@.                     .@@@. "
printf "$CYAN%b" "    .#######.          .#######.      .########.                         .@.                   .@@@.  "
printf "$CYAN%b" "   .#######.           .#######.        .########.                        .@@.                .@@@.   "
printf "$CYAN%b" "  .############################.          .########.                       .@@.              .@@@.    "
printf "$CYAN%b" " .#############################.            .########.                      .@@@.           .@@@.     "
printf "$CYAN%b" "                                                                             .@@@@@@@@@@@@@@@@@.      "
printf "\n"
printf "$YELLOW%b" "                     E K I V A L                                             G I M B A L A B L S    "
printf "$GREEN%b" "                  https://ekival.com                                        https://gimbalabs.com    "
printf "\n"
printf "$RED%b" "<------------------------------- CREATE Senate REFERENCE SCRIPT UTxO --------------------------------->"
printf "\n\n"

# Check Cardano Node is Running
check_process cardano-node

sleep 1

# Get Wallets UTxOs
if [[ -f "/tmp/republic_treasury.json" ]]; then
    rm /tmp/republic_treasury.json
fi
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting Republic Treasury Address UTxOs ..."
sleep 1
cardano-cli query utxo \
    --address "$REPUBLIC_TREASURY_ADDRESS" \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --out-file /tmp/republic_treasury.json
if [[ $(grep -q >/dev/null 2>&1) == $(grep '[^[:space:]]' /tmp/republic_treasury.json) && -f "/tmp/republic_treasury.json" ]]; then
    printf "\n$RED%b\n\n" "[-] ERROR: NO Any UTxOs Found At Republic Treasury Address"
    exit
fi
echo ""
cardano-cli query utxo \
    --address "$REPUBLIC_TREASURY_ADDRESS" \
    --testnet-magic "$MAGIC_TESTNET_NUMBER"
echo ""
TXNS=$(jq length /tmp/republic_treasury.json)
if [ "$TXNS" -eq "0" ]; then
    printf "\n$RED%b\n\n" "[-] ERROR: NO Any UTxOs Found At Republic Treasury Address"
    exit
fi
txin=$(jq -r --arg allTxIN "" 'keys[] | . + $allTxIN + " --tx-in"' /tmp/republic_treasury.json)
republicTreasuryRefTxIN=${txin::-8}
# republicTreasuryRefTxIN=$(get_address_biggest_lovelace "$REPUBLIC_TREASURY_ADDRESS")

sleep 1

# Get Minimum Amount
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Getting Minimum ADA Amount For Refrence Script UTxO ..."
sleep 1
cardano-cli transaction build \
    --babbage-era \
    --protocol-params-file "$WALLET_PATH"/protocol.json \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --out-file "$TX_PATH"/senate_ref_script.body \
    --change-address "$REPUBLIC_TREASURY_ADDRESS" \
    --tx-in ${republicTreasuryRefTxIN} \
    --tx-out="${REPUBLIC_TREASURY_ADDRESS} + 1000000" \
    --tx-out-reference-script-file "$SENATE_CONTRACT" \
    2>&1 | tee tmp >/dev/null 2>&1
minAmount=$(sed -n '2p' tmp)
IFS=' ' read -ra outputs <<<"$minAmount"
IFS=' ' read -ra minAmount <<<"${outputs[4]}"
echo -e "\033[1;32m[+] Minimum ADA Is Needed For Refrence Script UTxO:\033[0m" "${minAmount[0]}"
rm tmp

sleep 1

# Tx Output
contractRefOutput="$REPUBLIC_TREASURY_ADDRESS + ${minAmount[0]}"
echo -e "\033[1;32m[+] Tx Output to Republic Treasury Address: \033[0m" "$contractRefOutput"

sleep 1

# Contract Address
echo -e "\033[1;32m[+] The Senate Contract Address: \033[0m" "$SENATE_CONTRACT_ADDRESS"

sleep 1

# Build Tx
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Building Tx ..."
commandOutput=$(cardano-cli transaction build \
    --babbage-era \
    --protocol-params-file "$WALLET_PATH"/protocol.json \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --out-file "$TX_PATH"/senate_ref_script.body \
    --change-address "$REPUBLIC_TREASURY_ADDRESS" \
    --tx-in ${republicTreasuryRefTxIN} \
    --tx-out "$contractRefOutput" \
    --tx-out-reference-script-file "$SENATE_CONTRACT")
IFS=':' read -ra outputs <<<"$commandOutput"
IFS=' ' read -ra fee <<<"${outputs[1]}"
echo -e "\033[1;32m[+] Tx Fee:\033[0m" "${fee[1]}"

sleep 1

# Signing Tx
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Signing Tx ..."
cardano-cli transaction sign \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --out-file "$TX_PATH"/senate_ref_script.signed \
    --tx-body-file "$TX_PATH"/senate_ref_script.body \
    --signing-key-file "$REPUBLIC_TREASURY_PAYMENT_SKEY"

sleep 1

# Submit Tx
printf "$CYAN%b" "[$(date +%Y-%m-%d\ %H:%M:%S)] Submitting Tx ..."
commandOutput=$(cardano-cli transaction submit \
    --testnet-magic "$MAGIC_TESTNET_NUMBER" \
    --tx-file "$TX_PATH"/senate_ref_script.signed)
sleep 1
echo -e "\033[1;36m[$(date +%Y-%m-%d\ %H:%M:%S)]" "${commandOutput}"

sleep 1

# Tx ID
TXID=$(cardano-cli transaction txid --tx-file "$TX_PATH"/senate_ref_script.signed)
echo -e "\033[1;32m[+] Transaction ID:\033[0m" "$TXID"

sleep 1

printf "\n$RED%b\n\n" "<----------------------------------------------- DONE ------------------------------------------------>"

#
# Exit
#
