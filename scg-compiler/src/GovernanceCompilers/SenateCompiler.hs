{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE ImportQualifiedPost   #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}

module GovernanceCompilers.SenateCompiler
    (   mkSenateMintingPolicy
    ,   contractParams
    ,   writeScript
    )
        where


import           Cardano.Api.SerialiseTextEnvelope      (TextEnvelopeDescr,
                                                         textEnvelopeToJSON,
                                                         writeFileTextEnvelope)
import           Cardano.Api.Shelley                    (FileError,
                                                         PlutusScript (PlutusScriptSerialised),
                                                         PlutusScriptV2,
                                                         displayError)
import           Codec.Serialise                        (serialise)
import           Data.ByteString.Lazy                   (ByteString, toStrict)
import qualified Data.ByteString.Lazy                   as LBS
import           Data.ByteString.Short                  (ShortByteString,
                                                         length, toShort)
import           Data.List                              (break, intercalate,
                                                         reverse)
import           Data.List.Split                        (chunksOf)
import           Plutus.ApiCommon                       (ProtocolVersion (..))
import           Plutus.Script.Utils.Typed              (mkUntypedMintingPolicy)
import           Plutus.Script.Utils.V2.Scripts         (scriptHash)
import           Plutus.V1.Ledger.Scripts               (MintingPolicy, Script,
                                                         mkMintingPolicyScript,
                                                         scriptSize,
                                                         unMintingPolicyScript,
                                                         unScript)
import           Plutus.V2.Ledger.Api                   (ExBudget (..),
                                                         ExCPU (..),
                                                         ExMemory (..),
                                                         VerboseMode (Verbose),
                                                         evaluateScriptCounting,
                                                         exBudgetCPU,
                                                         exBudgetMemory,
                                                         mkEvaluationContext,
                                                         toData)
import           Plutus.V2.Ledger.Contexts              (ScriptContext (..))
import           PlutusCore                             (DefaultFun, DefaultUni,
                                                         Name, TyName,
                                                         defaultCostModelParams)
import           PlutusCore.Evaluation.Machine.ExMemory (CostingInteger)
import           PlutusCore.Pretty                      (Doc,
                                                         prettyPlcReadableDef)
import           PlutusTx                               (Data, applyCode,
                                                         compile, liftCode)
import           PlutusTx.Code                          (getPir, getPlc,
                                                         sizePlc)
import           Prelude                                (Bool (True), Char,
                                                         Either (Left, Right),
                                                         FilePath, IO, Integer,
                                                         Maybe (Just), Show,
                                                         error, fst, otherwise,
                                                         print, putStrLn, show,
                                                         snd, writeFile, ($),
                                                         (++), (.), (<>), (==))
import           System.Directory                       (createDirectoryIfMissing)
import           System.FilePath.Posix                  ((<.>), (</>))
import           UntypedPlutusCore                      (DeBruijn,
                                                         NamedDeBruijn, Program)

import qualified Plutonomy
import qualified PlutusIR

{-================================================================================================================================-}
{-==========                                             CONTRACT SECTION                                               ==========-}
{-================================================================================================================================-}

------------------------------------------------------------------------------------------
-- |                               CONTRACT IMPORTS                                   | --
------------------------------------------------------------------------------------------

import           GovernanceContracts.Senate             (senateMintingPolicy)
import           GovernanceToJSON.SenateToJSON          (designationRedeemer)
import           GovernanceTypes.SenateTypes            (SenateAction (..),
                                                         SenateParams (..))

------------------------------------------------------------------------------------------
-- |                          COMPILATION & OPTIMIZATION                              | --
------------------------------------------------------------------------------------------

mkSenateMintingPolicy :: SenateParams -> MintingPolicy
mkSenateMintingPolicy senateParams =
    Plutonomy.optimizeUPLC
        $   mkMintingPolicyScript
            $   $$(compile [|| mkUntypedMintingPolicy . senateMintingPolicy ||])
                `applyCode` liftCode senateParams

-- mkSenateMintingPolicy :: SenateParams -> MintingPolicy
-- mkSenateMintingPolicy senateParams =
--     Plutonomy.optimizeUPLCWith
--         Plutonomy.aggressiveOptimizerOptions
--         $   mkMintingPolicyScript
--             $   $$(compile [|| mkUntypedMintingPolicy . senateMintingPolicy ||])
--                 `applyCode` liftCode senateParams

------------------------------------------------------------------------------------------
-- |                               CONTRACT DETAILS                                   | --
------------------------------------------------------------------------------------------

type ContractParams = SenateParams

contractParams :: ContractParams
contractParams = SenateParams
    {   senatorsPKHs =
            [   "61aef8fc637e65b9fc862e45229b97d15e686bd463491f2cb003d113"
            ,   "aca061ea392296a8390152b59fb6dc8351df9da421083ea4ff3235a3"
            ,   "742ba6843a858d12dc8600ceab5d2b9ced8bd220b8cfc37d09c08273"
            ,   "19c6d778560e0a0fff7edcae3292c910e3403e031fac10043d7d7e67"
            ,   "49749e57887296bfaddc403966f12e0a2bbd6103bc73f2bcc0139634"
            ,   "64a8b1f900d30038e2c379e988457ae2ab09e014117d9fe064ef3d60"
            ,   "640e32f08e478d1eb0a56d02966f3d8f6fd22a54b5a371e4bb3a0f15"
            ]
    ,   caesarPKH           = "adb174eb050b41f8d8ab9532e2feed52626b1ef5703168caf3373ebb"
    ,   senateDebuggerPKH   = "a6a888d4e2f5245a8883b81a339173373bbf5d163b4be2cceff5673c"
    }

contractName :: [Char]
contractName = "Senate"

contractValidator
    :: SenateParams
    -> SenateAction
    -> ScriptContext
    -> Bool
contractValidator = senateMintingPolicy

contractMKScript :: Script
contractMKScript = unMintingPolicyScript $ mkSenateMintingPolicy contractParams

contractDescription :: TextEnvelopeDescr
contractDescription  = "..............."

------------------------------------------------------------------------------------------
-- |                               CONTRACT ExBUDGET                                  | --
------------------------------------------------------------------------------------------

red :: SenateAction
red = designationRedeemer

dataToCalculate :: [Data]
dataToCalculate = [toData red]

{-================================================== END OF CONTRACT SECTION =====================================================-}

{-================================================================================================================================-}
{-==========                                            GENERATOR SECTION                                               ==========-}
{-================================================================================================================================-}

writeCBORValidator :: FilePath -> Script -> IO (Either (FileError ()) ())
writeCBORValidator file = writeFileTextEnvelope @(PlutusScript PlutusScriptV2) file (Just contractDescription) . PlutusScriptSerialised . toShort . toStrict . serialise

writeOnlyJSON ::  Script -> ByteString
writeOnlyJSON = textEnvelopeToJSON @(PlutusScript PlutusScriptV2) (Just contractDescription) . PlutusScriptSerialised . toShort . toStrict . serialise


writeScript  :: IO ()
writeScript  = do
    let conDir          = "../dist/build/contracts"
        coresDir        = "../dist/data/core"
    createDirectoryIfMissing True conDir
    createDirectoryIfMissing True $ coresDir </> contractName
    result <- writeCBORValidator (conDir </> contractName <.> ".plutus") contractMKScript
    case result of
        Left err -> print $ displayError err
        Right () -> do
                        writeFile  (coresDir </> contractName </> "prettifiedTypedPlutusCore" <.> "txt") (show $ prettyPLC contractParams)
                        writeFile  (coresDir </> contractName </> "rawTypePlutusCore" <.> "txt") (show $ plc contractParams)
                        writeFile  (coresDir </> contractName </> "prettifiedUntypedPlutusCore" <.> "txt") (show prettyUPLC)
                        writeFile  (coresDir </> contractName </> "rawUntypedPlutusCore" <.> "txt") (show uplc)

                        putStrLn        "\n<----------------------------------------CONTRACT INFO----------------------------------------->\n"
                        putStrLn    $   "       Name:                       "   ++  contractName
                        putStrLn    $   "       Location:                   "   ++  conDir      </> contractName
                        putStrLn    $   "       Cores Location:             "   ++  coresDir    </> contractName
                        putStrLn    $   "       PLC Size (Bytes):           "   <>  show (format $  plcSize contractParams)
                        putStrLn    $   "       CBOR Size (Bytes):          "   <>  show (format $  LBS.length $ writeOnlyJSON contractMKScript)
                        putStrLn    $   "       CBOR Binary Size (Bytes):   "   <>  show (format $  length plutusCore)
                        putStrLn    $   "       Plutus Binary Size (Bytes): "   <>  show (format $  scriptSize contractMKScript)
                        putStrLn    $   "       CPU Usage (Step/PSec):      "   <>  show (format    getCPUExBudget)
                        putStrLn    $   "       Memory Usage (Bytes):       "   <>  show (format    getMemoryExBudget)
                        putStrLn    $   "       Script Hash:                "   <>  show (scriptHash contractMKScript)
                        putStrLn        "\n<---------------------------------------------------------------------------------------------->\n"

    where

        format :: Show a => a -> [Char]
        format x = h++t
            where
                sp = break (== '.') $ show x
                h = reverse (intercalate "," $ chunksOf 3 $ reverse $ fst sp)
                t = snd sp

        plutusCore :: ShortByteString
        plutusCore = toShort $ toStrict $ serialise contractMKScript

        uplc :: Program DeBruijn DefaultUni DefaultFun ()
        uplc = unScript contractMKScript

        plc :: ContractParams -> Program NamedDeBruijn DefaultUni DefaultFun ()
        plc cp = getPlc
            ($$(compile [|| contractValidator ||])
            `applyCode` liftCode cp)

        pir :: ContractParams -> Maybe (PlutusIR.Program TyName Name DefaultUni DefaultFun ())
        pir cp = getPir
            ($$(compile [||contractValidator ||])
            `applyCode` liftCode cp)

        plcSize :: ContractParams -> Integer
        plcSize cp = sizePlc
                ($$(compile [|| contractValidator ||])
                `applyCode` liftCode cp)

        prettyUPLC :: Doc ann0
        prettyUPLC = prettyPlcReadableDef uplc

        prettyPLC :: ContractParams -> Doc ann0
        prettyPLC cp = prettyPlcReadableDef $ plc cp

        vasilPV :: ProtocolVersion
        vasilPV =  ProtocolVersion 8 0

        scriptExBudget :: ExBudget
        scriptExBudget
            |   Just costModel  <-  defaultCostModelParams
            ,   Right model     <-  mkEvaluationContext costModel
            ,   ([], Right exB) <-  evaluateScriptCounting vasilPV Verbose model plutusCore dataToCalculate = exB
            |   otherwise = error "scriptExBudget Failed"

        getCPUExBudget :: CostingInteger
        getCPUExBudget
            |   (ExCPU unit) <- exBudgetCPU scriptExBudget = unit
            |   otherwise = 0

        getMemoryExBudget :: CostingInteger
        getMemoryExBudget
            |   (ExMemory unit) <- exBudgetMemory scriptExBudget = unit
            |   otherwise = 0

{-================================================= END OF GENERATOR SECTION =====================================================-}
