{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DeriveAnyClass        #-}
{-# LANGUAGE DeriveGeneric         #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE ImportQualifiedPost   #-}
{-# LANGUAGE LambdaCase            #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TemplateHaskell       #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE TypeOperators         #-}
{-# LANGUAGE UndecidableInstances  #-}


module ArenaOneCompilers.A1GateKeeperCompiler
    (   mkArena1'GateKeeperValidator
    ,   contractParams
    ,   writeScript
    )
        where

import           Cardano.Api.SerialiseTextEnvelope         (TextEnvelopeDescr,
                                                            textEnvelopeToJSON,
                                                            writeFileTextEnvelope)
import           Cardano.Api.Shelley                       (FileError,
                                                            PlutusScript (PlutusScriptSerialised),
                                                            PlutusScriptV2,
                                                            displayError)
import           Codec.Serialise                           (serialise)
import           Data.ByteString.Lazy                      (ByteString,
                                                            toStrict)
import qualified Data.ByteString.Lazy                      as LBS
import           Data.ByteString.Short                     (ShortByteString,
                                                            length, toShort)
import           Data.List                                 (break, intercalate,
                                                            reverse)
import           Data.List.Split                           (chunksOf)
import           Plutus.ApiCommon                          (ProtocolVersion (..))
import           Plutus.Script.Utils.Typed                 (mkUntypedValidator)
import           Plutus.Script.Utils.V2.Scripts            (scriptCurrencySymbol,
                                                            scriptHash,
                                                            validatorHash)
import           Plutus.V1.Ledger.Scripts                  (Script, Validator,
                                                            mkValidatorScript,
                                                            scriptSize,
                                                            unScript,
                                                            unValidatorScript)
import           Plutus.V2.Ledger.Api                      (ExBudget (..),
                                                            ExCPU (..),
                                                            ExMemory (..),
                                                            VerboseMode (Verbose),
                                                            evaluateScriptCounting,
                                                            exBudgetCPU,
                                                            exBudgetMemory,
                                                            mkEvaluationContext,
                                                            toData)
import           Plutus.V2.Ledger.Contexts                 (ScriptContext)
import           PlutusCore                                (DefaultFun,
                                                            DefaultUni, Name,
                                                            TyName,
                                                            defaultCostModelParams)
import           PlutusCore.Evaluation.Machine.ExMemory    (CostingInteger)
import           PlutusCore.Pretty                         (Doc,
                                                            prettyPlcReadableDef)
import           PlutusTx                                  (Data, applyCode,
                                                            compile, liftCode)
import           PlutusTx.Code                             (getPir, getPlc,
                                                            sizePlc)
import           Prelude                                   (Bool (True), Char,
                                                            Either (Left, Right),
                                                            FilePath, IO,
                                                            Integer,
                                                            Maybe (Just), Show,
                                                            error, fst,
                                                            otherwise, print,
                                                            putStrLn, show, snd,
                                                            writeFile, ($),
                                                            (++), (.), (<>),
                                                            (==))
import           System.Directory                          (createDirectoryIfMissing)
import           System.FilePath.Posix                     ((<.>), (</>))
import           UntypedPlutusCore                         (DeBruijn,
                                                            NamedDeBruijn,
                                                            Program)

import qualified Plutonomy
import qualified PlutusIR

{-================================================================================================================================-}
{-==========                                             CONTRACT SECTION                                               ==========-}
{-================================================================================================================================-}

------------------------------------------------------------------------------------------
-- |                               CONTRACT IMPORTS                                   | --
------------------------------------------------------------------------------------------

import qualified ArenaOneCompilers.A1DATMinterCompiler     as A1DATMinter
import           ArenaOneContracts.A1GateKeeper            (arena1'GateKeeperValidator)
import           ArenaOneToJSON.A1GateKeeperToJSON         (arena1'MasterDatum,
                                                            validateArena1'ContractsAtMintingA1'GATRedeemer)
import           ArenaOneTypes.A1GateKeeperTypes           (Arena1'GateKeeperAction (..),
                                                            Arena1'GateKeeperDatum (..),
                                                            Arena1'GateKeeperParams (..))
import qualified GovernanceCompilers.AccessControlCompiler as AccessControl

------------------------------------------------------------------------------------------
-- |                            COMPILATION & OPTIMIZATION                            | --
------------------------------------------------------------------------------------------

mkArena1'GateKeeperValidator :: Arena1'GateKeeperParams -> Validator
mkArena1'GateKeeperValidator arena1'GateKeeperParams = Plutonomy.optimizeUPLC
                                        $   mkValidatorScript
                                            $   $$(compile [|| mkUntypedValidator . arena1'GateKeeperValidator ||])
                                                `applyCode` liftCode arena1'GateKeeperParams

-- mkArena1'GateKeeperValidator :: Arena1'GateKeeperParams -> Validator
-- mkArena1'GateKeeperValidator arena1'GateKeeperParams = Plutonomy.optimizeUPLCWith
--                                         Plutonomy.aggressiveOptimizerOptions
--                                         $   mkValidatorScript
--                                             $   $$(compile [|| mkUntypedValidator . arena1'GateKeeperValidator ||])
--                                                 `applyCode` liftCode arena1'GateKeeperParams

------------------------------------------------------------------------------------------
-- |                               CONTRACT DETAILS                                   | --
------------------------------------------------------------------------------------------

type ContractParams = Arena1'GateKeeperParams

contractParams :: ContractParams
contractParams = Arena1'GateKeeperParams
    {   accessControlVH              =
            validatorHash $ AccessControl.mkAccessControlValidator AccessControl.contractParams
    ,   currencySymbolOfA1'DAT       =
            scriptCurrencySymbol
                $   A1DATMinter.mkArena1'DomainAuthTokenMintingPolicy A1DATMinter.contractParams
    ,   arena1'GateKeeperDebuggerPKH = "a6a888d4e2f5245a8883b81a339173373bbf5d163b4be2cceff5673c"

    }

contractName :: [Char]
contractName = "A1GateKeeper"

contractValidator
    :: Arena1'GateKeeperParams
    -> Arena1'GateKeeperDatum
    -> Arena1'GateKeeperAction
    -> ScriptContext
    -> Bool
contractValidator = arena1'GateKeeperValidator

contractMKScript :: Script
contractMKScript = unValidatorScript $ mkArena1'GateKeeperValidator contractParams

contractDescription :: TextEnvelopeDescr
contractDescription  = ".............."

------------------------------------------------------------------------------------------
-- |                               CONTRACT ExBUDGET                                  | --
------------------------------------------------------------------------------------------

red :: Arena1'GateKeeperAction
red = validateArena1'ContractsAtMintingA1'GATRedeemer

dat :: Arena1'GateKeeperDatum
dat = arena1'MasterDatum

dataToCalculate :: [Data]
dataToCalculate = [toData red, toData dat]

{-================================================== END OF CONTRACT SECTION =====================================================-}


{-================================================================================================================================-}
{-==========                                            GENERATOR SECTION                                               ==========-}
{-================================================================================================================================-}

writeCBORValidator :: FilePath -> Script -> IO (Either (FileError ()) ())
writeCBORValidator file = writeFileTextEnvelope @(PlutusScript PlutusScriptV2) file (Just contractDescription) . PlutusScriptSerialised . toShort . toStrict . serialise

writeOnlyJSON ::  Script -> ByteString
writeOnlyJSON = textEnvelopeToJSON @(PlutusScript PlutusScriptV2) (Just contractDescription) . PlutusScriptSerialised . toShort . toStrict . serialise


writeScript  :: IO ()
writeScript  = do
    let conDir          = "../dist/build/contracts"
        coresDir        = "../dist/data/core"
    createDirectoryIfMissing True conDir
    createDirectoryIfMissing True $ coresDir </> contractName
    result <- writeCBORValidator (conDir </> contractName <.> ".plutus") contractMKScript
    case result of
        Left err -> print $ displayError err
        Right () -> do
                        writeFile  (coresDir </> contractName </> "prettifiedTypedPlutusCore" <.> "txt") (show $ prettyPLC contractParams)
                        writeFile  (coresDir </> contractName </> "rawTypePlutusCore" <.> "txt") (show $ plc contractParams)
                        writeFile  (coresDir </> contractName </> "prettifiedUntypedPlutusCore" <.> "txt") (show prettyUPLC)
                        writeFile  (coresDir </> contractName </> "rawUntypedPlutusCore" <.> "txt") (show uplc)
                        -- writeFile  (coresDir </> contractName </> contractName <.> ".PIR") (show $ prettyPIR contractParams)

                        putStrLn        "\n<----------------------------------------CONTRACT INFO----------------------------------------->\n"
                        putStrLn    $   "       Name:                       "   ++  contractName
                        putStrLn    $   "       Location:                   "   ++  conDir      </> contractName
                        putStrLn    $   "       Cores Location:             "   ++  coresDir    </> contractName
                        putStrLn    $   "       PLC Size (Bytes):           "   <>  show (format $  plcSize contractParams)
                        putStrLn    $   "       CBOR Size (Bytes):          "   <>  show (format $  LBS.length $ writeOnlyJSON contractMKScript)
                        putStrLn    $   "       CBOR Binary Size (Bytes):   "   <>  show (format $  length plutusCore)
                        putStrLn    $   "       Plutus Binary Size (Bytes): "   <>  show (format $  scriptSize contractMKScript)
                        putStrLn    $   "       CPU Usage (Step/PSec):      "   <>  show (format    getCPUExBudget)
                        putStrLn    $   "       Memory Usage (Bytes):       "   <>  show (format    getMemoryExBudget)
                        putStrLn    $   "       Script Hash:                "   <>  show (scriptHash contractMKScript)
                        putStrLn        "\n<---------------------------------------------------------------------------------------------->\n"

                        -- putStrLn        "\n<---------------------------------UNTYPED PLUTUS CORE---------------------------------->\n"
                        -- print       $       prettyPlcReadableDef uplc
                        -- putStr          "\n<-------------------------------------------------------------------------------------->\n"

                        -- putStrLn        "\n<----------------------------------TYPED PLUTUS CORE----------------------------------->\n"
                        -- print       $       prettyPlcReadableDef $  contractParams
                        -- putStr          "\n<-------------------------------------------------------------------------------------->\n"

                        -- putStrLn        "\n<----------------------------------------PLUTUS IR------------------------------------->\n"
                        -- print       $       pretty $ fromJust (Just (pir contractParams))
                        -- putStr          "\n<-------------------------------------------------------------------------------------->\n"
    where

        format :: Show a => a -> [Char]
        format x = h++t
            where
                sp = break (== '.') $ show x
                h = reverse (intercalate "," $ chunksOf 3 $ reverse $ fst sp)
                t = snd sp

        plutusCore :: ShortByteString
        plutusCore = toShort $ toStrict $ serialise contractMKScript

        uplc :: Program DeBruijn DefaultUni DefaultFun ()
        uplc = unScript contractMKScript

        plc :: ContractParams -> Program NamedDeBruijn DefaultUni DefaultFun ()
        plc cp = getPlc
            ($$(compile [|| contractValidator ||])
            `applyCode` liftCode cp)

        pir :: ContractParams -> Maybe (PlutusIR.Program TyName Name DefaultUni DefaultFun ())
        pir cp = getPir
            ($$(compile [||contractValidator ||])
            `applyCode` liftCode cp)

        plcSize :: ContractParams -> Integer
        plcSize cp = sizePlc
                ($$(compile [|| contractValidator ||])
                `applyCode` liftCode cp)

        prettyUPLC :: Doc ann0
        prettyUPLC = prettyPlcReadableDef uplc

        prettyPLC :: ContractParams -> Doc ann0
        prettyPLC cp = prettyPlcReadableDef $ plc cp

        vasilPV :: ProtocolVersion
        vasilPV =  ProtocolVersion 8 0

        scriptExBudget :: ExBudget
        scriptExBudget
            |   Just costModel  <-  defaultCostModelParams
            ,   Right model     <-  mkEvaluationContext costModel
            ,   ([], Right exB) <-  evaluateScriptCounting vasilPV Verbose model plutusCore dataToCalculate = exB
            |   otherwise = error "scriptExBudget Failed"

        getCPUExBudget :: CostingInteger
        getCPUExBudget
            |   (ExCPU unit) <- exBudgetCPU scriptExBudget = unit
            |   otherwise = 0

        getMemoryExBudget :: CostingInteger
        getMemoryExBudget
            |   (ExMemory unit) <- exBudgetMemory scriptExBudget = unit
            |   otherwise = 0

        -- prettyPIR :: ContractParams -> Doc ann0
        -- prettyPIR cp = pretty $ fromMaybe (error "ERROR: No Plutus IR") (pir cp)

{-================================================= END OF GENERATOR SECTION =====================================================-}
