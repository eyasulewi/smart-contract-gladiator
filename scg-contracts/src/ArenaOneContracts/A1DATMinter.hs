
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}

module  ArenaOneContracts.A1DATMinter
            (   arena1'DomainAuthTokenMintingPolicy
            )
                where

import           Plutus.V1.Ledger.Address       (scriptHashAddress)
import           Plutus.V1.Ledger.Value         (flattenValue)
import           Plutus.V2.Ledger.Contexts      (ScriptContext (..),
                                                 TxInInfo (..), TxInfo (..),
                                                 TxOut (..), ownCurrencySymbol)
import           PlutusTx.Prelude               (Bool (..), all, any, elem,
                                                 filter, length, traceIfFalse,
                                                 (&&), (==))

import           ArenaOneTypes.A1DATMinterTypes (Arena1'DATAction (..),
                                                 Arena1'DATParams (..))

{-==============================================================================================================================-}
{-==========                                          MINTING POLICY SECTION                                          ==========-}
{-==============================================================================================================================-}

{-# INLINABLE arena1'DomainAuthTokenMintingPolicy #-}
arena1'DomainAuthTokenMintingPolicy :: Arena1'DATParams -> Arena1'DATAction -> ScriptContext -> Bool
arena1'DomainAuthTokenMintingPolicy Arena1'DATParams{..} redeemer ctx@ScriptContext{ scriptContextTxInfo = TxInfo{..}} =

    case redeemer of

        -----------------------------------------------------------------------------------------
        -- |                                   GOVERNOR MINT                                  |--
        -----------------------------------------------------------------------------------------

        GovernorMintA1'DAT ->
                traceIfFalse "Access Control Contract Must Be Present at Tx"
                        isAccessControlSCPresent
            &&  traceIfFalse "Only 1 A1'DAT Must Be Minted"
                        isMintedExactly

        ------------------------------------------------------------------------------------------
        -- |                                   GOVERNOR BURN                                   |--
        ------------------------------------------------------------------------------------------

        GovernorBurnA1'DAT ->
                traceIfFalse "Access Control Contract Must Be Present at Tx"
                        isAccessControlSCPresent
            &&  traceIfFalse "Only 1 A1'DAT Must Be Burnt"
                        isBurntExactly

        -----------------------------------------------------------------------------------------
        -- |                               DEBUGGER ACTION                                    |--
        -----------------------------------------------------------------------------------------

        A1'DATMinterDebugger ->
            traceIfFalse "A1'DATMinter Debugger Must Signnnnnnnn"
                (a1'DATMinterDebuggerPKH `elem` txInfoSignatories)

        where

            isMintedExactly ::  Bool
            isMintedExactly =
                    length (filter (\(cs,_,amt') -> cs == ownCurrencySymbol ctx && amt' == 1) (flattenValue txInfoMint)) == 1
                &&  length (flattenValue txInfoMint) == 1

            isBurntExactly ::  Bool
            isBurntExactly =
                all (\(cs,_,amt') -> cs == ownCurrencySymbol ctx && amt' == -1) (flattenValue txInfoMint)

            isAccessControlSCPresent :: Bool
            isAccessControlSCPresent =
                any (\i -> txOutAddress (txInInfoResolved i) == scriptHashAddress accessControlVH) txInfoInputs

{-============================================== END OF MINTING POLICY SECTION =================================================-}
