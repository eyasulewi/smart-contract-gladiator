
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}

module ArenaOneContracts.A1Proxy
    (   arena1'ProxyValidator
    )   where

import           Data.Maybe                 (fromMaybe)
import           Plutus.V1.Ledger.Address   (scriptHashAddress)
import           Plutus.V1.Ledger.Crypto    (PubKeyHash (..))
import           Plutus.V1.Ledger.Interval  (contains, to)
import           Plutus.V1.Ledger.Scripts   (Datum (..), Redeemer (..),
                                             ScriptHash (..),
                                             ValidatorHash (..))
import           Plutus.V1.Ledger.Time      (POSIXTime)
import           Plutus.V1.Ledger.Value     (CurrencySymbol (..),
                                             TokenName (..), Value (..))
import           Plutus.V2.Ledger.Contexts  (ScriptContext (..),
                                             ScriptPurpose (..), TxInInfo (..),
                                             TxInfo (..), TxOut (..),
                                             findOwnInput, getContinuingOutputs,
                                             ownHash)
import           Plutus.V2.Ledger.Tx        (OutputDatum (..))
import           PlutusTx                   (UnsafeFromData,
                                             unsafeFromBuiltinData)
import           PlutusTx.AssocMap          (lookup)
import           PlutusTx.Prelude           (Bool (..), Integer, Maybe (..),
                                             elem, find, otherwise, traceError,
                                             traceIfFalse, ($), (&&), (==))

import           ArenaOneTypes.A1ProxyTypes (Arena1'ProxyAction (..),
                                             Arena1'ProxyDatum (..),
                                             Arena1'ProxyParams (..))

{-==============================================================================================================================-}
{-==========                                            VALIDATOR SECTION                                             ==========-}
{-==============================================================================================================================-}

{-# INLINABLE arena1'ProxyValidator #-}
arena1'ProxyValidator :: Arena1'ProxyParams -> Arena1'ProxyDatum -> Arena1'ProxyAction -> ScriptContext -> Bool
arena1'ProxyValidator _ _ _ _ = True

{-================================================== END OF VALIDATOR SECTION ====================================================-}
