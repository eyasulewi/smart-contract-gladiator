
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}

module  ArenaOneContracts.A1GATMinter
            (   arena1'GladiatorAuthTokenMintingPolicy
            )
                where

import           Plutus.V1.Ledger.Address       (scriptHashAddress)
import           Plutus.V1.Ledger.Value         (flattenValue)
import           Plutus.V2.Ledger.Contexts      (ScriptContext (..),
                                                 TxInInfo (..), TxInfo (..),
                                                 TxOut (..), ownCurrencySymbol)
import           PlutusTx.Prelude               (Bool (..), all, any, elem,
                                                 filter, length, traceIfFalse,
                                                 (&&), (==))

import           ArenaOneTypes.A1GATMinterTypes (Arena1'GATAction (..),
                                                 Arena1'GATParams (..))

{-==============================================================================================================================-}
{-==========                                          MINTING POLICY SECTION                                          ==========-}
{-==============================================================================================================================-}

{-# INLINABLE arena1'GladiatorAuthTokenMintingPolicy #-}
arena1'GladiatorAuthTokenMintingPolicy :: Arena1'GATParams -> Arena1'GATAction -> ScriptContext -> Bool
arena1'GladiatorAuthTokenMintingPolicy Arena1'GATParams{..} redeemer ctx@ScriptContext{ scriptContextTxInfo = TxInfo{..}} =

    case redeemer of

        -----------------------------------------------------------------------------------------
        -- |                              GATE KEEPER ADMIN MINT                              |--
        -----------------------------------------------------------------------------------------

        GateKeeperAdminMintA1'GAT ->
                traceIfFalse "GateKeeper Contract Must Be Present at Tx"
                    isAccessControlSCPresent
            &&  traceIfFalse "Only 1 A1'DAT Must Be Minted"
                    isMintedExactly

        ------------------------------------------------------------------------------------------
        -- |                                     BURN A1GAT                                    |--
        ------------------------------------------------------------------------------------------

        BurnA1'GAT ->
            traceIfFalse "Only 1 A1'DAT Must Be Burnt"
                isBurntExactly

        -----------------------------------------------------------------------------------------
        -- |                               DEBUGGER ACTION                                    |--
        -----------------------------------------------------------------------------------------

        A1'GATMinterDebugger ->
            traceIfFalse "A1'GATMinter Debugger Must Signnnnnnnn"
                (a1'GATMinterDebuggerPKH `elem` txInfoSignatories)

        where

            isMintedExactly ::  Bool
            isMintedExactly =
                    length (filter (\(cs,_,amt') -> cs == ownCurrencySymbol ctx && amt' == 1) (flattenValue txInfoMint)) == 1
                &&  length (flattenValue txInfoMint) == 1

            isBurntExactly ::  Bool
            isBurntExactly =
                all (\(cs,_,amt') -> cs == ownCurrencySymbol ctx && amt' == -1) (flattenValue txInfoMint)

            isAccessControlSCPresent :: Bool
            isAccessControlSCPresent =
                any (\i -> txOutAddress (txInInfoResolved i) == scriptHashAddress gateKeeperVH) txInfoInputs

{-============================================== END OF MINTING POLICY SECTION =================================================-}
