
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}

module ArenaOneContracts.A1GateKeeper
    (   arena1'GateKeeperValidator
    )   where

import           Data.Maybe                         (fromMaybe)
import           Plutus.V1.Ledger.Address           (scriptHashAddress)
import           Plutus.V1.Ledger.Crypto            (PubKeyHash (..))
import           Plutus.V1.Ledger.Interval          (contains, to)
import           Plutus.V1.Ledger.Scripts           (Datum (..), Redeemer (..),
                                                     ScriptHash (..),
                                                     ValidatorHash (..))
import           Plutus.V1.Ledger.Time              (POSIXTime)
import           Plutus.V1.Ledger.Value             (CurrencySymbol (..),
                                                     TokenName (..), Value (..))
import           Plutus.V2.Ledger.Contexts          (ScriptContext (..),
                                                     ScriptPurpose (..),
                                                     TxInInfo (..), TxInfo (..),
                                                     TxOut (..), findOwnInput,
                                                     getContinuingOutputs,
                                                     ownHash)
import           Plutus.V2.Ledger.Tx                (OutputDatum (..))
import           PlutusTx                           (UnsafeFromData,
                                                     unsafeFromBuiltinData)
import           PlutusTx.AssocMap                  (lookup)
import           PlutusTx.Prelude                   (Bool (..), Integer,
                                                     Maybe (..), elem, find,
                                                     otherwise, traceError,
                                                     traceIfFalse, ($), (&&),
                                                     (==), (||))

import           ArenaOneTypes.A1GATMinterTypes     (Arena1'GATAction (..))
import           ArenaOneTypes.A1GateKeeperTypes    (Arena1'GateKeeperAction (..),
                                                     Arena1'GateKeeperDatum (..),
                                                     Arena1'GateKeeperParams (..))
import           ArenaOneTypes.A1RefUTxOIssuerTypes (Arena1'RefUTxOIssuerDatum (..))
import           GovernanceTypes.AccessControlTypes (AccessControlAction (..))
import           GovernanceTypes.RBACTypes          (Assignment (..),
                                                     Domain (..), Role (..))

{-==============================================================================================================================-}
{-==========                                            VALIDATOR SECTION                                             ==========-}
{-==============================================================================================================================-}

{-# INLINABLE arena1'GateKeeperValidator #-}
arena1'GateKeeperValidator :: Arena1'GateKeeperParams -> Arena1'GateKeeperDatum -> Arena1'GateKeeperAction -> ScriptContext -> Bool
arena1'GateKeeperValidator Arena1'GateKeeperParams{..} datum redeemer ctx@ScriptContext{ scriptContextTxInfo = TxInfo{..} } =

    case ( redeemer , datum ) of

            -----------------------------------------------------------------------------------------
            -- |                  VALIDATE ARENA 1 CONTRACTS AT MINTING A1'GAT                    |--
            -----------------------------------------------------------------------------------------

            ( ValidateArena1'ContractsAtMintingA1'GAT,
                arena1'GateKeeperAdminDatum@Arena1'GateKeeperAdminDatum{arena1'GateKeeperAdminAssignment = Assignment Arena1 ArenaMaster, ..} )
                    |   arena1'GateKeeperAdminDatum' <- getTxOutInlineDatum singleScriptOutputUTxO
                    ,   Arena1'GateKeeperRefUTxODatum
                            _
                            _
                            currencySymbolOfArena1'GAT <- getTxOutInlineDatum $ fromJust $ getRefUTxOTxOut ownScriptHash
                    ,   GateKeeperAdminMintA1'GAT <- getArena1'GATRedeemer currencySymbolOfArena1'GAT
                            ->
                                validation
                                    arena1'GateKeeperAdminPKH
                                    currencySymbolOfArena1'GAT
                                    Nothing
                                    arena1'GateKeeperAdminDatum
                                    arena1'GateKeeperAdminDatum'
                                    arena1'GateKeeperAdminSession

            -----------------------------------------------------------------------------------------
            -- |                 VALIDATE ARENA 1 CONTRACTS AT BATTLE FIELD                       |--
            -----------------------------------------------------------------------------------------

            ( ValidateArena1'ContractsAtBattlefield,
                arena1'GateKeeperAdminDatum@Arena1'GateKeeperAdminDatum{arena1'GateKeeperAdminAssignment = Assignment Arena1 ArenaMaster, ..} )
                    |   arena1'GateKeeperAdminDatum' <- getTxOutInlineDatum singleScriptOutputUTxO
                    ,   Arena1'GateKeeperRefUTxODatum
                            _
                            arena1'ProxySH
                            currencySymbolOfArena1'GAT <- getTxOutInlineDatum $ fromJust $ getRefUTxOTxOut ownScriptHash
                                ->
                                    validation
                                        arena1'GateKeeperAdminPKH
                                        currencySymbolOfArena1'GAT
                                        (Just arena1'ProxySH)
                                        arena1'GateKeeperAdminDatum
                                        arena1'GateKeeperAdminDatum'
                                        arena1'GateKeeperAdminSession

            -----------------------------------------------------------------------------------------
            -- |                   ARENA 1 GLADIATORS CONTRACTS REVOCATION                        |--
            -----------------------------------------------------------------------------------------

            ( Arena1'GladiatorsRevocation,
                arena1'GateKeeperAdminDatum@Arena1'GateKeeperAdminDatum{arena1'GateKeeperAdminAssignment = Assignment Arena1 Undertaker, ..} )
                |   arena1'GateKeeperAdminDatum' <- getTxOutInlineDatum singleScriptOutputUTxO
                ,   Arena1'GateKeeperRefUTxODatum
                        _
                        arena1'ProxySH
                        currencySymbolOfArena1'GAT <- getTxOutInlineDatum $ fromJust $ getRefUTxOTxOut ownScriptHash
                ,   BurnA1'GAT <- getArena1'GATRedeemer currencySymbolOfArena1'GAT
                        ->
                            validation
                                arena1'GateKeeperAdminPKH
                                currencySymbolOfArena1'GAT
                                (Just arena1'ProxySH)
                                arena1'GateKeeperAdminDatum
                                arena1'GateKeeperAdminDatum'
                                arena1'GateKeeperAdminSession


            -----------------------------------------------------------------------------------------
            -- |              REVOKE REFERENCE UTxOs & ARENA ADMINS AT GATE KEEPER                |--
            -----------------------------------------------------------------------------------------

            ( Arena1'GateKeeperRevocation, _)
                |   RevokeGateKeeper <- getAccessControlRedeemer -> True


            -----------------------------------------------------------------------------------------
            -- |                       ARENA 1 GATE KEEPER DEBUGGER ACTION                        |--
            -----------------------------------------------------------------------------------------

            ( Arena1'GateKeeperDebugger
                , _
                ) ->
                    traceIfFalse "ARENA 1 Gate Keeper Debugger Must Signnnnnnnn"
                        $ arena1'GateKeeperDebuggerPKH  `elem` txInfoSignatories

            -----------------------------------------------------------------------------------------
            -- |                                 ANY OTHER CASE                                   |--
            -----------------------------------------------------------------------------------------

            ( _
                , _
                ) ->
                    traceIfFalse "Arena1'GateKeeper 1" False

            where

                validation
                    :: PubKeyHash
                    -> CurrencySymbol
                    -> Maybe ScriptHash
                    -> Arena1'GateKeeperDatum
                    -> Arena1'GateKeeperDatum
                    -> POSIXTime
                    -> Bool
                validation adminPKH a1'GATMinterCS arena1'ProxySH dat dat' adminSession =
                        traceIfFalse "Evaluation of A1'DAT Failed"      (evaluationOfA1'DAT (pkhToTokenName adminPKH))
                    &&  traceIfFalse "Arena Master Must Sign Tx"        (adminPKH `elem` txInfoSignatories)
                    &&  traceIfFalse "One or All SC Missing From Tx"    areSCsPresent
                    &&  traceIfFalse "Datum Mismatched"                 (dat == dat')
                    -- &&  traceIfFalse "Session Has Been Expired"         (to adminSession `contains` txInfoValidRange)
                        where
                            evaluationOfA1'DAT :: TokenName -> Bool
                            evaluationOfA1'DAT tn =
                                    valueOf (txOutValue singleScriptInputUTxO) currencySymbolOfA1'DAT tn == 1
                                &&  valueOf (txOutValue singleScriptOutputUTxO) currencySymbolOfA1'DAT tn == 1
                                &&  txOutValue singleScriptInputUTxO == txOutValue singleScriptOutputUTxO

                            areSCsPresent :: Bool
                            areSCsPresent =
                                case arena1'ProxySH of
                                    Just arena1'ProxySH'  | Just _ <- getRefUTxOTxOut arena1'ProxySH'
                                                    , Just _ <- getRefUTxOTxOut (currencySymbolToScriptHash a1'GATMinterCS)
                                                    -> True
                                                    | otherwise -> traceError "Arena1'GateKeeper 2"
                                    Nothing         | Just _ <- getRefUTxOTxOut (currencySymbolToScriptHash a1'GATMinterCS)
                                                    -> True
                                                    | otherwise -> traceError "Arena1'GateKeeper 3"

                            pkhToTokenName :: PubKeyHash -> TokenName
                            pkhToTokenName pkh =
                                TokenName $ getPubKeyHash pkh

                fromJust :: Maybe a -> a
                fromJust (Just x) = x
                fromJust _        = traceError "Arena1'GateKeeper 4"

                currencySymbolToScriptHash :: CurrencySymbol -> ScriptHash
                currencySymbolToScriptHash (CurrencySymbol hash) = ScriptHash hash

                ownScriptHash :: ScriptHash
                ownScriptHash = validatorHashToScriptHash $ ownHash ctx
                    where
                        validatorHashToScriptHash :: ValidatorHash -> ScriptHash
                        validatorHashToScriptHash (ValidatorHash hash) = ScriptHash hash

                getTxOutInlineDatum :: forall dat. (UnsafeFromData dat) => TxOut -> dat
                getTxOutInlineDatum tx
                    | ( OutputDatum ( Datum inline )) <- txOutDatum tx
                    = unsafeFromBuiltinData @dat inline
                    | otherwise = traceError "Arena1'GateKeeper 5"

                getAccessControlRedeemer :: AccessControlAction
                getAccessControlRedeemer
                    |   Just i <- find (\i -> txOutAddress (txInInfoResolved i) == scriptHashAddress accessControlVH) txInfoInputs
                    ,   Just rawMintingRedeemer <- lookup (Spending (txInInfoOutRef i)) txInfoRedeemers
                    =   unsafeFromBuiltinData @AccessControlAction $ getRedeemer rawMintingRedeemer
                    |   otherwise = traceError "Arena1'GateKeeper 6"

                getArena1'GATRedeemer :: CurrencySymbol -> Arena1'GATAction
                getArena1'GATRedeemer cs
                    |   Just rawMintingRedeemer <- lookup (Minting cs) txInfoRedeemers
                    =   unsafeFromBuiltinData @Arena1'GATAction $ getRedeemer rawMintingRedeemer
                    |   otherwise = traceError "Arena1'GateKeeper 7"

                valueOf :: Value -> CurrencySymbol -> TokenName -> Integer
                valueOf (Value mp) cur tn
                    |   Just i <- lookup cur mp = fromMaybe 0 (lookup tn i)
                    |   otherwise = 0

                singleScriptOutputUTxO :: TxOut
                singleScriptOutputUTxO
                    |   [o]         <-  getContinuingOutputs ctx = o
                    |   otherwise   =   traceError "Arena1'GateKeeper 7"

                singleScriptInputUTxO :: TxOut
                singleScriptInputUTxO
                    |   Just i      <-  findOwnInput ctx = txInInfoResolved i
                    |   otherwise   =   traceError "Arena1'GateKeeper 8"

                getRefUTxOTxOut :: ScriptHash -> Maybe TxOut
                getRefUTxOTxOut sh
                    |   Just i <- find (    \i ->
                                                    txOutAddress (txInInfoResolved i) == scriptHashAddress (ownHash ctx)
                                                &&
                                                    fromJust (txOutReferenceScript (txInInfoResolved i)) == sh
                                                &&
                                                    valueOf (txOutValue (txInInfoResolved i)) currencySymbolOfA1'DAT (scriptHashToTokenName sh) == 1
                                        )
                                        txInfoReferenceInputs = Just $ txInInfoResolved i
                    |   otherwise = traceError "Arena1'GateKeeper 9"
                            where
                                scriptHashToTokenName :: ScriptHash -> TokenName
                                scriptHashToTokenName (ScriptHash hash) = TokenName hash

{-================================================== END OF VALIDATOR SECTION ====================================================-}
