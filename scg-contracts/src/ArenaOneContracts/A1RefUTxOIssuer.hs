
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}

module ArenaOneContracts.A1RefUTxOIssuer
    (   arena1'RefUTxOIssuerValidator
    )   where

import           Data.Maybe                         (fromMaybe)
import           Plutus.V1.Ledger.Address           (Address (..),
                                                     scriptHashAddress)
import           Plutus.V1.Ledger.Scripts           (Datum (..), Redeemer (..),
                                                     ScriptHash (..),
                                                     ValidatorHash (..))
import           Plutus.V1.Ledger.Value             (CurrencySymbol (..),
                                                     TokenName (..), Value (..))
import           Plutus.V2.Ledger.Contexts          (ScriptContext (..),
                                                     ScriptPurpose (..),
                                                     TxInInfo (..), TxInfo (..),
                                                     TxOut (..), findOwnInput,
                                                     getContinuingOutputs,
                                                     ownHash)
import           Plutus.V2.Ledger.Tx                (OutputDatum (..))
import           PlutusTx                           (UnsafeFromData,
                                                     unsafeFromBuiltinData)
import           PlutusTx.AssocMap                  (lookup)
import           PlutusTx.Prelude                   (Bool (..), Integer,
                                                     Maybe (..), any, elem,
                                                     find, otherwise,
                                                     traceError, traceIfFalse,
                                                     ($), (&&), (==))

import           ArenaOneTypes.A1RefUTxOIssuerTypes (Arena1'RefUTxOIssuerAction (..),
                                                     Arena1'RefUTxOIssuerDatum (..),
                                                     Arena1'RefUTxOIssuerParams (..))
import           GovernanceTypes.AccessControlTypes (AccessControlAction (..),
                                                     AccessControlDatum (..),
                                                     Arena1'DomainInfo (..),
                                                     TreasuryInfo (..))

{-==============================================================================================================================-}
{-==========                                            VALIDATOR SECTION                                             ==========-}
{-==============================================================================================================================-}

{-# INLINABLE arena1'RefUTxOIssuerValidator #-}
arena1'RefUTxOIssuerValidator ::  Arena1'RefUTxOIssuerParams -> Arena1'RefUTxOIssuerDatum -> Arena1'RefUTxOIssuerAction -> ScriptContext -> Bool
arena1'RefUTxOIssuerValidator Arena1'RefUTxOIssuerParams{..} _ redeemer ctx@ScriptContext{ scriptContextTxInfo = TxInfo{..} } =
    case redeemer of

        -----------------------------------------------------------------------------------------
        -- |                      ISSUE ARENA 1 GATE KEEPER REFERENCE UTxO                    |--
        -----------------------------------------------------------------------------------------

        IssueArena1'GateKeeperRefUTxO
            |   AccessControlRefUTxODatum
                    TreasuryInfo{republicTreasuryAddress}
                    Arena1'DomainInfo{arena1'RefUTxOIssuerVH,arena1'GateKeeperVH,arena1'ProxyVH,currencySymbolOfA1'DAT,currencySymbolOfA1'GAT}
                    _ _ _ <- getTxOutInlineDatum accessControlRefTxOut
            ,   Arena1'GateKeeperRefUTxODatum republicTreasuryAddress' arena1'ProxySH currencySymbolOfA1'GAT' <- getTxOutInlineDatum $ getValidatorHashTxOut arena1'GateKeeperVH
            ,   Arena1'AccessControlRefUTxOManagement <- getAccessControlRedeemer
                    ->
                        traceIfFalse "SC Input and Output not matched"
                            isSCInputEqualOutput
                    &&  traceIfFalse "Arena 1 Gate Keeper TxOut Has Wrong A1'DAT (arena1'GateKeeperVH)"
                            (isSCTxOutHasAuthToken arena1'GateKeeperVH currencySymbolOfA1'DAT (validatorHashToTokenName arena1'GateKeeperVH))
                    &&  traceIfFalse "arena1'RefUTxOIssuerVH Mismatched"
                            (arena1'RefUTxOIssuerVH == ownHash ctx )
                    &&  traceIfFalse "republicTreasuryAddress Mismatched"
                            (republicTreasuryAddress == republicTreasuryAddress' )
                    &&  traceIfFalse "arena1'ProxyVH & arena1'ProxySH Mismatched"
                            (arena1'ProxySH == validatorHashToScriptHash arena1'ProxyVH )
                    &&  traceIfFalse "currencySymbolOfA1'GAT Mismatched"
                            (currencySymbolOfA1'GAT' == currencySymbolOfA1'GAT)

        -----------------------------------------------------------------------------------------
        -- |                         ISSUE ARENA 1 PROXY REFERENCE UTxO                       |--
        -----------------------------------------------------------------------------------------

        IssueArena1'ProxyRefUTxO
            |   AccessControlRefUTxODatum _ Arena1'DomainInfo{..} _ _ _ <- getTxOutInlineDatum accessControlRefTxOut
            ,   Arena1'ProxyRefUTxODatum currencySymbolOfA1'GAT' <- getTxOutInlineDatum $ getValidatorHashTxOut arena1'GateKeeperVH
            ,   Arena1'AccessControlRefUTxOManagement <- getAccessControlRedeemer
                    ->
                            traceIfFalse "SC Input and Output not matched"
                                isSCInputEqualOutput
                        &&  traceIfFalse "Arena 1 Gate Keeper TxOut Has Wrong A1'DAT (arena1'ProxyVH)"
                                (isSCTxOutHasAuthToken arena1'GateKeeperVH currencySymbolOfA1'DAT (validatorHashToTokenName arena1'ProxyVH))
                        &&  traceIfFalse "arena1'RefUTxOIssuerVH Mismatched"
                                (arena1'RefUTxOIssuerVH == ownHash ctx)
                        &&  traceIfFalse "currencySymbolOfA1'GAT Mismatched"
                                (currencySymbolOfA1'GAT == currencySymbolOfA1'GAT')

        -----------------------------------------------------------------------------------------
        -- |                          ISSUE A1'GAT REFERENCE UTxO                             |--
        -----------------------------------------------------------------------------------------

        IssueA1'GATMinterRefUTxO
            |   AccessControlRefUTxODatum _ Arena1'DomainInfo{..} _ _ _ <- getTxOutInlineDatum accessControlRefTxOut
            ,   A1'GATMinterRefUTxODatum arena1'ProxyVH' <- getTxOutInlineDatum $ getValidatorHashTxOut arena1'GateKeeperVH
            ,   Arena1'AccessControlRefUTxOManagement <- getAccessControlRedeemer
                    ->
                            traceIfFalse "SC Input and Output not matched"
                                isSCInputEqualOutput
                        &&  traceIfFalse "Arena 1 Gate Keeper TxOut Has Wrong A1'DAT (A1'DATMinter)"
                                (isSCTxOutHasAuthToken arena1'GateKeeperVH currencySymbolOfA1'DAT (currencySymbolToTokenName currencySymbolOfA1'GAT))
                        &&  traceIfFalse "arena1'RefUTxOIssuerVH Mismatched"
                                (arena1'RefUTxOIssuerVH == ownHash ctx )
                        &&  traceIfFalse "arena1'ProxyVH Mismatched"
                                (arena1'ProxyVH == arena1'ProxyVH')

        -----------------------------------------------------------------------------------------
        -- |                     SEND SCRIPT ACTIVATOR UTxO TO TREASURY                      | --
        -----------------------------------------------------------------------------------------

        SendBackScriptActivatorUTxO
            |   AccessControlRefUTxODatum TreasuryInfo{..} _ _ _ _ <- getTxOutInlineDatum accessControlRefTxOut
                    ->
                        traceIfFalse "isAddrGettingPaidExactly error"
                            (isAddrGettingPaidExactly republicTreasuryAddress)

        -----------------------------------------------------------------------------------------
        -- |                    ARENA 1 REFERENCE UTxO ISSUER DEBUGGER ACTION                     |--
        -----------------------------------------------------------------------------------------

        Arena1'RefUTxOIssuerDebugger ->
            traceIfFalse "Arena 1 Ref UTxO Issuer Debugger Must Signnnnnnnn"
                $   arena1'RefUTxOIssuerDebuggerPKH `elem` txInfoSignatories

        -----------------------------------------------------------------------------------------
        -- |                                 ANY OTHER CASE                                   |--
        -----------------------------------------------------------------------------------------

        _ -> traceIfFalse "Arena1'RefUTxOIssuer 1" False

        where

            validatorHashToTokenName :: ValidatorHash -> TokenName
            validatorHashToTokenName (ValidatorHash hash) = TokenName hash

            validatorHashToScriptHash :: ValidatorHash -> ScriptHash
            validatorHashToScriptHash (ValidatorHash hash) = ScriptHash hash

            currencySymbolToTokenName :: CurrencySymbol -> TokenName
            currencySymbolToTokenName (CurrencySymbol hash) = TokenName hash

            getTxOutInlineDatum :: forall dat. (UnsafeFromData dat) => TxOut -> dat
            getTxOutInlineDatum TxOut{txOutDatum}
                | ( OutputDatum ( Datum inline )) <- txOutDatum
                = unsafeFromBuiltinData @dat inline
                | otherwise = traceError "Arena1'RefUTxOIssuer 2"

            getAccessControlRedeemer :: AccessControlAction
            getAccessControlRedeemer
                |   Just i <- find (\i -> txOutAddress (txInInfoResolved i) == scriptHashAddress accessControlVH) txInfoInputs
                ,   Just rawMintingRedeemer <- lookup (Spending (txInInfoOutRef i)) txInfoRedeemers
                =   unsafeFromBuiltinData @AccessControlAction $ getRedeemer rawMintingRedeemer
                |   otherwise = traceError "Arena1'RefUTxOIssuer 3"

            getValidatorHashTxOut :: ValidatorHash -> TxOut
            getValidatorHashTxOut vh
                |   Just o <- find (\o -> txOutAddress o == scriptHashAddress vh) txInfoOutputs = o
                |   otherwise = traceError "Arena1'RefUTxOIssuer 4"

            valueOf :: Value -> CurrencySymbol -> TokenName -> Integer
            valueOf (Value mp) cur tn
                |   Just i <- lookup cur mp = fromMaybe 0 (lookup tn i)
                |   otherwise = 0

            singleScriptOutputUTxO :: TxOut
            singleScriptOutputUTxO
                |   [o]         <-  getContinuingOutputs ctx = o
                |   otherwise   =   traceError "Arena1'RefUTxOIssuer 5"

            singleScriptInputUTxO :: TxOut
            singleScriptInputUTxO
                |   Just i      <-  findOwnInput ctx = txInInfoResolved i
                |   otherwise   =   traceError "Arena1'RefUTxOIssuer 6"

            isSCInputEqualOutput :: Bool
            isSCInputEqualOutput =
                txOutValue singleScriptOutputUTxO == txOutValue singleScriptInputUTxO

            -- accessControlRefTxOut :: CurrencySymbol -> TokenName -> TxOut
            -- accessControlRefTxOut cs tn
            --     |   Just i <- find (\ i -> valueOf (txOutValue (txInInfoResolved i)) senatePolicyID (validatorHashToTokenName accessControlVH) == 1) txInfoReferenceInputs = txInInfoResolved i
            --     |   otherwise = traceError "Arena1'RefUTxOIssuer 7"

            accessControlRefTxOut :: TxOut
            accessControlRefTxOut
                |   Just i <- find (\ i ->
                                                txOutAddress (txInInfoResolved i) == scriptHashAddress accessControlVH
                                            &&
                                                fromJust (txOutReferenceScript (txInInfoResolved i)) == validatorHashToScriptHash accessControlVH
                                            &&
                                                valueOf (txOutValue (txInInfoResolved i)) senatePolicyID (validatorHashToTokenName accessControlVH) == 1)
                                    txInfoReferenceInputs = txInInfoResolved i
                |   otherwise = traceError "Arena1'RefUTxOIssuer 8"
                        where
                            fromJust :: Maybe a -> a
                            fromJust (Just x) = x
                            fromJust _        = traceError "Arena1'RefUTxOIssuer 9"

            isSCTxOutHasAuthToken :: ValidatorHash ->  CurrencySymbol -> TokenName -> Bool
            isSCTxOutHasAuthToken vh cs tn =
                valueOf (txOutValue $ getValidatorHashTxOut vh) cs tn == 1

            isAddrGettingPaidExactly :: Address -> Bool
            isAddrGettingPaidExactly addr =
                any (\o -> txOutAddress o == addr && txOutValue o == txOutValue singleScriptInputUTxO) txInfoOutputs

{-================================================== END OF VALIDATOR SECTION ====================================================-}
