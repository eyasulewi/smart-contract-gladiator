
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NamedFieldPuns        #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeApplications      #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}

module GovernanceContracts.AccessControl
            (   accessControlValidator
            )
                where

import           Data.Maybe                         (fromMaybe)
import           Plutus.V1.Ledger.Address           (Address (..),
                                                     scriptHashAddress)
import           Plutus.V1.Ledger.Crypto            (PubKeyHash (..))
import           Plutus.V1.Ledger.Interval          (contains, from, to)
import           Plutus.V1.Ledger.Scripts           (Datum (..), Redeemer (..),
                                                     ScriptHash (..),
                                                     ValidatorHash (..))
import           Plutus.V1.Ledger.Time              (POSIXTime)
import           Plutus.V1.Ledger.Value             (CurrencySymbol (..),
                                                     TokenName (..), Value (..),
                                                     adaSymbol, adaToken,
                                                     symbols)
import           Plutus.V2.Ledger.Contexts          (ScriptContext (..),
                                                     ScriptPurpose (..),
                                                     TxInInfo (..), TxInfo (..),
                                                     TxOut (..), findOwnInput,
                                                     getContinuingOutputs,
                                                     ownHash)
import           Plutus.V2.Ledger.Tx                (OutputDatum (..))
import           PlutusTx                           (UnsafeFromData,
                                                     unsafeFromBuiltinData)
import           PlutusTx.AssocMap                  (lookup)
import           PlutusTx.Prelude                   (Bool (..), Integer,
                                                     Maybe (..), all, any, elem,
                                                     find, map, otherwise, sum,
                                                     traceError, traceIfFalse,
                                                     ($), (&&), (.), (==))

import           ArenaOneTypes.A1DATMinterTypes     (Arena1'DATAction (..))
import           ArenaOneTypes.A1GateKeeperTypes    (Arena1'GateKeeperDatum (..))
import           GovernanceTypes.AccessControlTypes (AccessControlAction (..),
                                                     AccessControlDatum (..),
                                                     AccessControlParams (..),
                                                     Arena1'DomainInfo (..),
                                                     TreasuryInfo (..))
import           GovernanceTypes.RBACTypes          (Assignment (..),
                                                     Domain (..))
import           GovernanceTypes.SenateTypes        (SenateAction (..))

{-==============================================================================================================================-}
{-==========                                            VALIDATOR SECTION                                             ==========-}
{-==============================================================================================================================-}

{-# INLINABLE accessControlValidator #-}
accessControlValidator :: AccessControlParams -> AccessControlDatum -> AccessControlAction -> ScriptContext -> Bool
accessControlValidator AccessControlParams{..} datum redeemer ctx@ScriptContext{ scriptContextTxInfo = TxInfo{..} }  =

    case ( redeemer , datum ) of

        -----------------------------------------------------------------------------------------
        -- |                  REFERENCE UTxO MANAGEMENT FOR ARENA 1 DOMAIN                    |--
        -----------------------------------------------------------------------------------------

        ( Arena1'AccessControlRefUTxOManagement, governorDatum@GovernorDatum{..})
            |   governorDatum' <- getTxOutInlineDatum singleScriptOutputUTxO
            ,   AccessControlRefUTxODatum _ Arena1'DomainInfo{arena1'RefUTxOIssuerVH,currencySymbolOfA1'DAT} _ _ _ <- getTxOutInlineDatum ownScriptRefTxOut
            ,   GovernorMintA1'DAT <- getMinterRedeemer currencySymbolOfA1'DAT
                    ->
                        governorEvaluation
                            governorPKH
                            governorSession
                            governorDatum
                            governorDatum'

                        &&  traceIfFalse "Ref Maker Is Not Present At Tx"
                                (isSCPresentAtTx arena1'RefUTxOIssuerVH)

        -----------------------------------------------------------------------------------------
        -- |                       DESIGNATE ARENA 1 GATE KEEPER ADMIN                        |--
        -----------------------------------------------------------------------------------------

        ( DesignateGateKeeper, governorDatum@GovernorDatum{..})
            |   governorDatum' <- getTxOutInlineDatum singleScriptOutputUTxO
            ,   AccessControlRefUTxODatum
                    _
                    Arena1'DomainInfo{arena1'GateKeeperVH, currencySymbolOfA1'DAT}
                    _
                    _
                    _ <- getTxOutInlineDatum ownScriptRefTxOut
            ,   GovernorMintA1'DAT <- getMinterRedeemer currencySymbolOfA1'DAT
            ,   Arena1'GateKeeperAdminDatum arena1'GateKeeperAdminPKH (Assignment Arena1 _) arena1'GateKeeperAdminSession <- getTxOutInlineDatum $ getValidatorHashTxOut arena1'GateKeeperVH
                    ->
                        governorEvaluation
                            governorPKH
                            governorSession
                            governorDatum
                            governorDatum'

                        &&  traceIfFalse "Arena Master UTxO Does Not Have A1'DAT Token"
                                (isContractTxOutHasTheToken arena1'GateKeeperVH currencySymbolOfA1'DAT (pkhToTokenName arena1'GateKeeperAdminPKH))
                        &&  traceIfFalse "Arena Master Session Is Wrong"
                                (from arena1'GateKeeperAdminSession `contains` txInfoValidRange)

        -----------------------------------------------------------------------------------------
        -- |                            REVOKE ARENA 1 GATE KEEPER                            |--
        -----------------------------------------------------------------------------------------

        ( RevokeGateKeeper, governorDatum@GovernorDatum{..})
            |   governorDatum' <- getTxOutInlineDatum singleScriptOutputUTxO
            ,   AccessControlRefUTxODatum
                    TreasuryInfo{republicTreasuryAddress}
                    Arena1'DomainInfo{arena1'GateKeeperVH, currencySymbolOfA1'DAT}
                    _
                    _
                    _ <- getTxOutInlineDatum ownScriptRefTxOut
            ,   GovernorBurnA1'DAT <- getMinterRedeemer currencySymbolOfA1'DAT
                    ->
                        governorEvaluation
                            governorPKH
                            governorSession
                            governorDatum
                            governorDatum'

                        &&  traceIfFalse "All Inputs from Arena 1 Gate Keeper Must Have A1'DAT Token"
                                (isAllGateKeeperInputsHasAuthToken arena1'GateKeeperVH currencySymbolOfA1'DAT)
                        &&  traceIfFalse "Treasury Must Get Paid Exactly"
                                (isAddrGettingPaidExactly arena1'GateKeeperVH republicTreasuryAddress)


        -----------------------------------------------------------------------------------------
        -- |                                REVOKE GOVERNOR                                   |--
        -----------------------------------------------------------------------------------------

        ( RevokeGovernor, GovernorDatum{} )
            |   AccessControlRefUTxODatum
                    TreasuryInfo{romanEmperorTreasuryAddress}
                    _
                    _
                    _
                    _ <- getTxOutInlineDatum ownScriptRefTxOut
            ,   Revocation <- getMinterRedeemer senatePolicyID
                    ->
                        traceIfFalse "Treasury Must Get Paid Exactly"
                            (isAddrGettingPaidExactly (ownHash ctx) romanEmperorTreasuryAddress)


        -----------------------------------------------------------------------------------------
        -- |                      REVOKE ACCESS CONTROL REFERENCE UTxO                        |--
        -----------------------------------------------------------------------------------------

        ( RevokeAccessControlRefUTxO, AccessControlRefUTxODatum{} )
            |   Revocation <- getMinterRedeemer senatePolicyID -> True

        -----------------------------------------------------------------------------------------
        -- |                         ACCESS CONTROL DEBUGGER ACTION                           |--
        -----------------------------------------------------------------------------------------

        ( AccessControlDebugger, _) ->
            traceIfFalse "Access Control Debugger Must Signnnnnnnn"
                $   accessControlDebuggerPKH `elem` txInfoSignatories

        -----------------------------------------------------------------------------------------
        -- |                                 ANY OTHER CASE                                   |--
        -----------------------------------------------------------------------------------------

        ( _ , _) -> traceIfFalse "AccessControl 1" False

        where

            governorEvaluation :: PubKeyHash -> POSIXTime -> AccessControlDatum -> AccessControlDatum -> Bool
            governorEvaluation governorPKH governorSession dat dat' =
                    traceIfFalse "AccessControl 2"
                        (evaluationOfSenatePolicyID $ pkhToTokenName governorPKH)
                &&  traceIfFalse "AccessControl 3"
                        (governorPKH `elem` txInfoSignatories)
                -- &&  traceIfFalse "AccessControl 4"
                --         (to governorSession `contains` txInfoValidRange)
                &&  traceIfFalse "AccessControl 5"
                        (dat == dat')
                    where
                        evaluationOfSenatePolicyID :: TokenName -> Bool
                        evaluationOfSenatePolicyID tn =
                                valueOf (txOutValue singleScriptInputUTxO) senatePolicyID tn == 1
                            &&  valueOf (txOutValue singleScriptOutputUTxO) senatePolicyID tn == 1
                            &&  txOutValue singleScriptInputUTxO == txOutValue singleScriptOutputUTxO

                        singleScriptInputUTxO :: TxOut
                        singleScriptInputUTxO
                            |   Just i      <-  findOwnInput ctx = txInInfoResolved i
                            |   otherwise   =   traceError "AccessControl 6"

            pkhToTokenName :: PubKeyHash -> TokenName
            pkhToTokenName pkh =
                TokenName $ getPubKeyHash pkh

            getTxOutInlineDatum :: forall dat. (UnsafeFromData dat) => TxOut -> dat
            getTxOutInlineDatum tx
                | ( OutputDatum ( Datum inline )) <- txOutDatum tx
                = unsafeFromBuiltinData @dat inline
                | otherwise = traceError "AccessControl 7"

            getMinterRedeemer :: forall rad. (UnsafeFromData rad) => CurrencySymbol -> rad
            getMinterRedeemer cs
                |   Just rawMintingRedeemer <- lookup (Minting cs) txInfoRedeemers
                =   unsafeFromBuiltinData @rad $ getRedeemer rawMintingRedeemer
                |   otherwise = traceError "AccessControl 8"

            getValidatorHashTxOut :: ValidatorHash -> TxOut
            getValidatorHashTxOut vh
                |   Just o <- find (\o -> txOutAddress o == scriptHashAddress vh) txInfoOutputs = o
                |   otherwise = traceError "AccessControl 9"

            valueOf :: Value -> CurrencySymbol -> TokenName -> Integer
            valueOf (Value mp) cur tn
                |   Just i <- lookup cur mp = fromMaybe 0 (lookup tn i)
                |   otherwise = 0

            singleScriptOutputUTxO :: TxOut
            singleScriptOutputUTxO
                |   [o]         <-  getContinuingOutputs ctx = o
                |   otherwise   =   traceError "AccessControl 10"

            ownScriptRefTxOut :: TxOut
            ownScriptRefTxOut
                |   Just i <- find (\ i ->
                                                    txOutAddress (txInInfoResolved i) == scriptHashAddress (ownHash ctx)
                                            &&
                                                    fromJust (txOutReferenceScript (txInInfoResolved i)) == validatorHashToScriptHash (ownHash ctx)
                                            &&
                                                    valueOf (txOutValue (txInInfoResolved i)) senatePolicyID (validatorHashToTokenName (ownHash ctx)) == 1)
                                    txInfoReferenceInputs = txInInfoResolved i
                |   otherwise = traceError "AccessControl 11"
                        where
                            fromJust :: Maybe a -> a
                            fromJust (Just x) = x
                            fromJust _        = traceError "AccessControl 12"

                            validatorHashToTokenName :: ValidatorHash -> TokenName
                            validatorHashToTokenName (ValidatorHash hash) = TokenName hash

                            validatorHashToScriptHash :: ValidatorHash -> ScriptHash
                            validatorHashToScriptHash (ValidatorHash hash) = ScriptHash hash

            isSCPresentAtTx :: ValidatorHash -> Bool
            isSCPresentAtTx  vh =
                any (\i -> txOutAddress (txInInfoResolved i) == scriptHashAddress vh) txInfoInputs

            isContractTxOutHasTheToken :: ValidatorHash ->  CurrencySymbol -> TokenName -> Bool
            isContractTxOutHasTheToken vh cs tn =
                valueOf (txOutValue $ getValidatorHashTxOut vh) cs tn == 1

            isAllGateKeeperInputsHasAuthToken :: ValidatorHash -> CurrencySymbol -> Bool
            isAllGateKeeperInputsHasAuthToken  vh cs =
                all (\val -> cs `elem` symbols val) allOwnInputValue
                    where
                        allOwnInputValue = [txOutValue (txInInfoResolved i) | i <- txInfoInputs, txOutAddress (txInInfoResolved i) == scriptHashAddress vh ]

            isAddrGettingPaidExactly :: ValidatorHash -> Address -> Bool
            isAddrGettingPaidExactly vh addr
                |   Just o <- find ((== addr) . txOutAddress ) txInfoOutputs
                ,   allVHInputValue <- [txOutValue (txInInfoResolved i) | i <- txInfoInputs, txOutAddress (txInInfoResolved i) == scriptHashAddress vh ]
                ,   sumOfAllInputs <- sum $ map adaAmountOf allVHInputValue
                =   adaAmountOf (txOutValue o) == sumOfAllInputs
                |   otherwise = traceError "AccessControl 13"
                        where
                            adaAmountOf :: Value -> Integer
                            adaAmountOf val = valueOf val adaSymbol adaToken


{-================================================== END OF VALIDATOR SECTION ====================================================-}
