
{-# LANGUAGE DataKinds             #-}
{-# LANGUAGE DerivingVia           #-}
{-# LANGUAGE FlexibleContexts      #-}
{-# LANGUAGE GADTs                 #-}
{-# LANGUAGE MultiParamTypeClasses #-}
{-# LANGUAGE NoImplicitPrelude     #-}
{-# LANGUAGE OverloadedStrings     #-}
{-# LANGUAGE RankNTypes            #-}
{-# LANGUAGE RecordWildCards       #-}
{-# LANGUAGE ScopedTypeVariables   #-}
{-# LANGUAGE TypeFamilies          #-}
{-# LANGUAGE UndecidableInstances  #-}

module  GovernanceContracts.Senate
            (   senateMintingPolicy
            )
                where

import           Plutus.V1.Ledger.Value      (flattenValue)
import           Plutus.V2.Ledger.Contexts   (ScriptContext (..), TxInfo (..),
                                              ownCurrencySymbol)
import           PlutusTx.Builtins           (divideInteger)
import           PlutusTx.Prelude            (Bool (..), all, elem, filter,
                                              length, traceIfFalse, ($), (&&),
                                              (+), (==), (||))

import           GovernanceTypes.SenateTypes (SenateAction (..),
                                              SenateParams (..))

{-==============================================================================================================================-}
{-==========                                          MINTING POLICY SECTION                                          ==========-}
{-==============================================================================================================================-}

{-# INLINABLE senateMintingPolicy #-}
senateMintingPolicy :: SenateParams -> SenateAction -> ScriptContext -> Bool
senateMintingPolicy SenateParams{..} redeemer ctx@ScriptContext{ scriptContextTxInfo = TxInfo{..}} =

    case redeemer of

            -----------------------------------------------------------------------------------------
            -- |                             DESIGNATE ACCESS CONTROL                             |--
            -----------------------------------------------------------------------------------------

            Designation ->
                    traceIfFalse "All Senate Members And Caesar Must Sign Tx"
                        (       all (`elem` txInfoSignatories) senatorsPKHs
                            &&
                                caesarPKH `elem` txInfoSignatories
                        )
                &&  traceIfFalse "Only 1 Senate Token Must Be Minted"
                        isMintedExactly

            -----------------------------------------------------------------------------------------
            -- |                             REVOKE ACCESS CONTROL                                |--
            -----------------------------------------------------------------------------------------

            Revocation ->
                    traceIfFalse "%51 of Senate Members Or Caesar Must Sign Tx"
                        (       length (filter (`elem` txInfoSignatories) senatorsPKHs) == (length senatorsPKHs `divideInteger` 2) + 1
                            ||
                                caesarPKH `elem` txInfoSignatories
                        )
                &&  traceIfFalse "Only 1 Senate Token Must Be Burnt"
                        isBurntExactly

            -----------------------------------------------------------------------------------------
            -- |                             REVOKE ACCESS CONTROL                                |--
            -----------------------------------------------------------------------------------------

            Veto ->
                    traceIfFalse "Caesar Must Sign Tx"
                        (caesarPKH `elem` txInfoSignatories)
                &&  traceIfFalse "Only 1 Senate Token Must Be Burnt"
                        isBurntExactly

            -----------------------------------------------------------------------------------------
            -- |                             SENATE DEBUGGER ACTION                               |--
            -----------------------------------------------------------------------------------------

            SenateDebugger ->
                traceIfFalse "Senate Debugger Must Signnnnnnnnn"
                    $ senateDebuggerPKH `elem` txInfoSignatories

            where

                isMintedExactly ::  Bool
                isMintedExactly =
                        length (filter (\(cs,_,amt') -> cs == ownCurrencySymbol ctx && amt' == 1) (flattenValue txInfoMint)) == 1
                    &&  length (flattenValue txInfoMint) == 1

                isBurntExactly ::  Bool
                isBurntExactly =
                    all (\(cs,_,amt') -> cs == ownCurrencySymbol ctx && amt' == -1) (flattenValue txInfoMint)

{-============================================== END OF MINTING POLICY SECTION =================================================-}
